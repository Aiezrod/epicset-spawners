package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.stores.location.EpicLocationStore;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class UpdateBlockCommand extends SubCommands {

	private String name = "updateblock";
	private LocationStore locations;
	public UpdateBlockCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods,LocationStore locations) {
		super(plugin, store,nmsMethods);
		this.locations=locations;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(CommonUtils.chat("&cYou Dont Have Permission."));
			return;
		}
		if(sub.length==2) {
			Block block=getFacingBlock(sender);
			if(block==null||!(block.getState() instanceof CreatureSpawner)) {
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease look at a spawner."));
				return;
			}
			SpawnerData oldSpawner;
			if(locations.isStored(block))
				oldSpawner=locations.fetch(block);
			else {
				CreatureSpawner creatureSpawner=(CreatureSpawner)block.getState();
				oldSpawner=store.getSpawner(creatureSpawner.getSpawnedType());
			}
			if(sub[1].equalsIgnoreCase("default")) {
				block.breakNaturally();
				oldSpawner.applyToBlock(block, null);
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully changed "+CommonUtils.format(((EpicSpawnerData)oldSpawner).getName())+" spawner to it's default value."));
				return;
			}
			else if(store.isSpawner(sub[1].toLowerCase())) {
				SpawnerData newSpawner=store.getSpawner(sub[1].toLowerCase());
				block.breakNaturally();
				newSpawner.applyToBlock(block, null);
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully changed all "+CommonUtils.format(((EpicSpawnerData)oldSpawner).getName())+" spawners to "+CommonUtils.format(((EpicSpawnerData)newSpawner).getName())+"."));
				return;
			}
			else {
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo spawner by that name found."));
				return;
			}
		}
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length>=3) {
			plugin.getSettings().reload();
			if(store.isSpawner(sub[1].toLowerCase())&&store.isSpawner(sub[2].toLowerCase())) {
				SpawnerData oldSpawner=store.getSpawner(sub[1].toLowerCase());
				SpawnerData newSpawner=store.getSpawner(sub[2].toLowerCase());
				for(Block block:((EpicLocationStore)locations).getAllBlocks()) {
					if(locations.fetch(block).isSimilar(oldSpawner)) {
						block.breakNaturally();
						newSpawner.applyToBlock(block, null);
					}
				}
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully changed all "+CommonUtils.format(((EpicSpawnerData)oldSpawner).getName())+" spawners to "+CommonUtils.format(((EpicSpawnerData)newSpawner).getName())+"."));
				return;
			}
			else if(store.isSpawner(sub[1].toLowerCase())&&sub[2].equalsIgnoreCase("default")) {
				SpawnerData oldSpawner=store.getSpawner(sub[1].toLowerCase());
				for(Block block:((EpicLocationStore)locations).getAllBlocks()) {
					if(locations.fetch(block).isSimilar(oldSpawner)) {
						block.breakNaturally();
						oldSpawner.applyToBlock(block, null);
					}
				}
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully changed all "+CommonUtils.format(((EpicSpawnerData)oldSpawner).getName())+" spawners to their default values."));
				return;
			}
			else {
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo spawner by that name found."));
				return;
			}
		}
		sendHelp(sender);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if (length == 2) {
			List<String> result = new ArrayList<>();
			List<String> options = ((EpicSpawnerStore)store).getAllSpawners();
			options.add("default");
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		if (length == 3) {
			List<String> result = new ArrayList<>();
			List<String> options = ((EpicSpawnerStore)store).getAllSpawners();
			options.add("default");
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {

		if(sender instanceof Player) {
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fUpdate already placed spawners :"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock default   &7Update facing spawner to default values."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock <spawner name>   &7Update facing spawner to new spawner name"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock <target name> default   &7Update all target spawners to default values."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock <target name> <spawner name>   &7Update all target spawners to new spawner name"));
		} else {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fUpdate already placed spawners :"));
			sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock <target name> default   &7Update all target spawners to default values."));
			sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock <target name> <spawner name>   &7Update all target spawners to new spawner name"));
		}
	}

	private Block getFacingBlock(Player player) {
		BlockIterator bi=new BlockIterator(player,10);
		Block lastBlock;
		while (bi.hasNext()) {
			lastBlock=bi.next();
			if(lastBlock!=null&&lastBlock.getState() instanceof CreatureSpawner)
				return lastBlock;
		}
		return null;
	}
}
