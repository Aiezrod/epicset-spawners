package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class ApplyDefaultCommand extends SubCommands {

	private String name = "applydefault";

	public ApplyDefaultCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods) {
		super(plugin, store,nmsMethods);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(CommonUtils.chat("&cYou Dont Have Permission."));
			return;
		}
		onCommandByConsole(sender, sub);
	}
	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length>=2) {
			plugin.getSettings().reload();
			if(sub[1].equalsIgnoreCase("all")) {
				new BukkitRunnable() {
					@Override
					public void run() {
						((EpicSpawnerStore)store).defaultAll();
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully applied default values to all spawners."));
					}
				}.runTaskAsynchronously(plugin);
				return;
			}
			else if(store.isSpawner(sub[1].toLowerCase())) {
				((EpicSpawnerStore)store).applyDefault(sub[1].toLowerCase());
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully applied default values to "+CommonUtils.format(sub[1])+"'s spawners."));
				return;
			}
			else {
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo spawner by that name found."));
				return;
			}
		}
		sendHelp(sender);
		return;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if (length == 2) {
			List<String> result = new ArrayList<>();
			List<String> options = ((EpicSpawnerStore)store).getAllSpawners();
			options.add("all");
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}

		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fApply default values to a spawner :"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner applydefault all   &7Apply to all spawners"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner applydefault <spawner name>   &7Apply to spawner"));
	}
}
