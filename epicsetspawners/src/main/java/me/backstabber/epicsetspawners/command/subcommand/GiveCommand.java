package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.SpawnerData.SpawnerParameters;
import me.backstabber.epicsetspawners.api.data.UpgradeData.DataType;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.JSONMessage;

public class GiveCommand extends SubCommands {

	private String name = "give";

	public GiveCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods) {
		super(plugin, store,nmsMethods);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(CommonUtils.chat("&cYou Dont Have Permission."));
			return;
		}
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {

		if (sub.length <= 3) {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		if (!store.isSpawner(sub[1])) {
			sender.sendMessage(
					CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo spawner by name " + sub[1] + " found."));
			return;
		}
		SpawnerData spawner = store.getSpawner(sub[1]);
		if (Bukkit.getPlayerExact(sub[2]) == null) {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo player by name " + sub[2] + " found."));
			return;
		}
		Player player = Bukkit.getPlayerExact(sub[2]);
		List<String> subs=new ArrayList<String>();
		for(int i=3;i<sub.length;i++) {
			subs.add(0,sub[i]);
		}
		List<Integer> numbers;
		try {
			numbers = getNumbers(subs);
		} catch (NumberFormatException e) {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cIncorrect amount " + e.getMessage() + " specified."));
			return;
		}
		if(numbers.size()>=6) {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cToo many arguments specified."));
			return;
		}
		int amount = (numbers.size()>=1) ? numbers.get(0):1;
		int stack = (numbers.size()>=2) ? numbers.get(1):1;
		int minDelay = (numbers.size()>=3) ? numbers.get(2):200;
		int maxDelay = (numbers.size()>=4) ? numbers.get(3):500;
		int mobCount = (numbers.size()>=5) ? numbers.get(4):5;
		amount = Math.abs(amount);
		stack = Math.abs(stack);
		minDelay = Math.abs(minDelay);
		maxDelay = Math.abs(maxDelay);
		mobCount = Math.abs(mobCount);
		if(maxDelay<=minDelay) {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cMaximum delay must be greater than minimum delay."));
			return;
		}
		spawner.changeParameter(SpawnerParameters.AMOUNT, mobCount);
		((EpicSpawnerData) spawner).setStack(stack);
		spawner.getUpgradeData().changeParameter(DataType.MAX_DELAY, maxDelay);
		spawner.getUpgradeData().changeParameter(DataType.MIN_DELAY, minDelay);
		spawner.getUpgradeData().changeParameter(DataType.MOB_COUNT, mobCount);
		ItemStack item = spawner.getItemStack();
		for (int i = 0; i < amount; i++)
			CommonUtils.giveItem(player, item);
		JSONMessage message=JSONMessage.create(CommonUtils.chat(
				"&6&lEpicSet-Spawners &7&l>&fGave " + CommonUtils.getItemName(item) + "&f to &6" + player.getName()));
		JSONMessage tip=JSONMessage.create(CommonUtils.chat("&e----------------"))
				.newline().then(CommonUtils.chat("&7Name : &e"+((EpicSpawnerData)spawner).getName()))
				.newline().then(CommonUtils.chat("&7Stack Size : &e"+stack))
				.newline().then(CommonUtils.chat("&7Amount : &e"+amount))
				.newline().then(CommonUtils.chat("&7Min Delay : &e"+CommonUtils.getTimeFormat(minDelay/20)))
				.newline().then(CommonUtils.chat("&7Max Delay : &e"+CommonUtils.getTimeFormat(maxDelay/20)))
				.newline().then(CommonUtils.chat("&7Spawn Count : &e"+mobCount))
				.newline().then(CommonUtils.chat("&e----------------"))
				;
		if(sender instanceof Player) {
			message.tooltip(tip).send((Player)sender);
		}
		else
			sender.sendMessage(CommonUtils.chat(
					"&6&lEpicSet-Spawners &7&l>&fGave " + CommonUtils.getItemName(item) + "&f to &6" + player.getName()));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {

		if (length == 2) {
			List<String> result = new ArrayList<>();
			List<String> options = ((EpicSpawnerStore) store).getAllSpawners();
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		if (length == 3) {
			List<String> result = new ArrayList<>();
			List<String> options = new ArrayList<String>();
			for (Player player : Bukkit.getOnlinePlayers())
				options.add(player.getName());
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}

		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {

		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fGive usage :"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner give <spawner name> <player> <amount>"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner give <spawner name> <player> <stack size> <amount>"));
		sender.sendMessage(
				CommonUtils.chat("&6&l>&f/espawner give <spawner name> <player> <min delay> <stack size> <amount>"));
		sender.sendMessage(CommonUtils
				.chat("&6&l>&f/espawner give <spawner name> <player> <max delay> <min delay> <stack size> <amount>"));
		sender.sendMessage(CommonUtils.chat(
				"&6&l>&f/espawner give <spawner name> <player> <mob count> <max delay> <min delay> <stack size> <amount>"));
		sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Mob & spawner cant have space use _ instead."));
		sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Stack size is used to give already stacked spawners."));
		sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Min delay & max delay are delays btw spawning."));
		sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Mob count is the no of mobs spawned at a time"));
	}
}
