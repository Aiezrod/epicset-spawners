package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class DeleteCommand extends SubCommands {

	private String name = "delete";

	public DeleteCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods) {
		super(plugin, store,nmsMethods);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(CommonUtils.chat("&cYou Dont Have Permission."));
			return;
		}
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length>=2) {
			String name=sub[1];
			if(!store.isCustomSpawner(name)) {
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo spawner by name "+name+" found."));
				return;
			}
			((EpicSpawnerStore)store).removeSpawner(name);
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aSucessfully deleted spawner "+name+"."));
			return;
		}
		sendHelp(sender);
		return;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		alias.add("remove");
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if (length == 2) {
			List<String> result = new ArrayList<>();
			List<String> options = ((EpicSpawnerStore)store).getCustomSpawners();
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}

		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fSpawners Remover Usage :"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner delete <name>   &7Delete a spawner"));
	}
}
