package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.builder.VariableBuilder;
import me.backstabber.epicsetspawners.api.builder.validate.ChangeBy;
import me.backstabber.epicsetspawners.api.builder.validate.VariableTypes;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.UpgradeData.DataType;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;

@SuppressWarnings("deprecation")
public class CreateCommand extends SubCommands implements  Listener {

	private String name = "create";
	private Map<Player, SpawnerBuilder> variableBuilding=new HashMap<Player, SpawnerBuilder>();
	public CreateCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods) {
		super(plugin, store,nmsMethods);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(CommonUtils.chat("&cYou Dont Have Permission."));
			return;
		}
		if(sub.length>=2) {
			SpawnerType type;
			try {
				type=SpawnerType.valueOf(sub[1].toUpperCase());
			} catch (Exception e) {
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cIncorrect type specified."));
				return;
			}
			if(type.equals(SpawnerType.BLOCK)) {
				if(sub.length>=3&&sub[2].equalsIgnoreCase("byhand")) {
					if(sub.length<4) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter name of spawner."));
						return;
					}
					String name=sub[3].toLowerCase();
					ItemStack item=sender.getInventory().getItemInHand();
					if(item==null||!item.getType().isBlock()) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease hold a block to create a spawner."));
						return;
					}
					if(store.isSpawner(name)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerData spawner = SpawnerBuilder.createDefault(type, name).setData(DataType.MOB_COUNT, 1).setEntity(EntityType.DROPPED_ITEM).setNbtTags(EpicMaterials.valueOf(item).name()).saveToFile().getSpawner();
					((EpicSpawnerStore)store).addSpawner(name, spawner);
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(type.name())+" spawner named "+CommonUtils.format(name)+"."));
					return;
				}
				if(sub.length>=3&&sub[2].equalsIgnoreCase("byname")) {
					if(sub.length<5) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter name of block & spawner."));
						return;
					}
					String name=sub[4].toLowerCase();
					ItemStack item;
					try {
						item=EpicMaterials.valueOf(sub[3].toUpperCase()).getItemStack();
					} catch (Exception e) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease Enter correct block name."));
						return;
					}
					if(!item.getType().isBlock()) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cThat is not a block."));
						return;
					}
					if(store.isSpawner(name)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerData spawner = SpawnerBuilder.createDefault(type, name).setData(DataType.MOB_COUNT, 1).setEntity(EntityType.DROPPED_ITEM).setNbtTags(EpicMaterials.valueOf(item).name()).saveToFile().getSpawner();
					((EpicSpawnerStore)store).addSpawner(name, spawner);
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(type.name())+" spawner named "+CommonUtils.format(name)+"."));
					return;
				}
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreate Solid block spawner :"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create block byhand <name>   &fCreate from block in hand"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create block byname <block_name> <name>   &fCreate from block name"));
				sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Spawner & block name cant have space use _ instead."));
				sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7For block name use 1.14 names"));
				return;
			}
			if(type.equals(SpawnerType.CUSTOM_MOB)) {
				if(sub.length>=3&&sub[2].equalsIgnoreCase("bylook")) {
					if(sub.length<4) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter name of spawner."));
						return;
					}
					String name=sub[3].toLowerCase();
					Entity entity=getEntity(sender);
					if(entity==null) {
						sender.sendMessage(CommonUtils.chat("&6&lDivictusSpawners &7&l>&cPlease look at a mob."));
						return;
					}
					if(store.isSpawner(name)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerData spawner = SpawnerBuilder.createDefault(type, name).setEntity(entity.getType()).setNbtTags(nmsMethods.getEntityNbt(entity)).saveToFile().getSpawner();
					((EpicSpawnerStore)store).addSpawner(name, spawner);
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(type.name())+" spawner named "+CommonUtils.format(name)+"."));
					return;
				}
				if(sub.length>=3&&sub[2].equalsIgnoreCase("byname")) {
					if(sub.length<5) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter name of item & spawner."));
						return;
					}
					String name=sub[4].toLowerCase();
					EntityType entity;
					try {
							entity=EntityType.valueOf(sub[3].toUpperCase());
					} catch (Exception e) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease Enter correct mob name."));
						return;
					}
					if(store.isSpawner(name)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerData spawner = SpawnerBuilder.createDefault(type, name).setEntity(entity).setNbtTags("{}").saveToFile().getSpawner();
					((EpicSpawnerStore)store).addSpawner(name, spawner);
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(type.name())+" spawner named "+CommonUtils.format(name)+"."));
					return;
				}
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreate Item Spawners :"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create item byhand <name>   &fCreate from item in hand"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create item byname <item_name> <name>   &fCreate from item name"));
				sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Spawner & item name cant have space use _ instead."));
				sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7For item name use 1.14 names"));
				return;
			}
			if(type.equals(SpawnerType.ITEM)) {
				if(sub.length>=3&&sub[2].equalsIgnoreCase("byhand")) {
					if(sub.length<4) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter name of spawner."));
						return;
					}
					String name=sub[3].toLowerCase();
					ItemStack item=sender.getInventory().getItemInHand();
					if(item==null||item.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial())) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease hold an item to create a spawner."));
						return;
					}
					if(store.isSpawner(name)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerData spawner = SpawnerBuilder.createDefault(type, name).setEntity(EntityType.DROPPED_ITEM).setNbtTags(nmsMethods.getItemNbt(item)).saveToFile().getSpawner();
					((EpicSpawnerStore)store).addSpawner(name, spawner);
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(type.name())+" spawner named "+CommonUtils.format(name)+"."));
					return;
				}
				if(sub.length>=3&&sub[2].equalsIgnoreCase("byname")) {
					if(sub.length<5) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter name of item & spawner."));
						return;
					}
					String name=sub[4].toLowerCase();
					ItemStack item;
					try {
						item=EpicMaterials.valueOf(sub[3].toUpperCase()).getItemStack();
					} catch (Exception e) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease Enter correct item name."));
						return;
					}
					if(store.isSpawner(name)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerData spawner = SpawnerBuilder.createDefault(type, name).setEntity(EntityType.DROPPED_ITEM).setNbtTags(nmsMethods.getItemNbt(item)).saveToFile().getSpawner();
					((EpicSpawnerStore)store).addSpawner(name, spawner);
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(type.name())+" spawner named "+CommonUtils.format(name)+"."));
					return;
				}
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreate Mob Spawners :"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create custom_mob bylook <name>   &fCreate from mob you face"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create custom_mob byname <mob_name> <name>   &fCreate from mob name"));
				sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Spawner & mob name cant have space use _ instead."));
				return;
			}

			if(type.equals(SpawnerType.VARIABLE)) {
				if(sub.length>=4) {
					String spawnerName = sub[2].toLowerCase();
					if(!store.isSpawner(spawnerName)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cNo spawner by name "+CommonUtils.format(spawnerName)+" found."));
						return;
					}
					SpawnerData spawner = store.getSpawner(spawnerName);
					if(spawner.getType().equals(SpawnerType.VARIABLE)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cCannot use a vairable spawner as default."));
						return;
					}
					String newName = sub[3].toLowerCase();
					if(store.isSpawner(newName)) {
						sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cA spawner by that name already exists."));
						return;
					}
					SpawnerBuilder builder = SpawnerBuilder.load(spawner.getType(), ((EpicSpawnerData)spawner).getName()).setType(SpawnerType.VARIABLE)
							.setName(newName)
							.setVariable(VariableBuilder.create());
					this.variableBuilding.put(sender, builder);
					sender.setMetadata("EpicSpawnerBuilding", new FixedMetadataValue(plugin, "type"));
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fStarted building a variable spawner."));
					sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fType change type for spawner in chat. (placeholder,time,spawn)"));
					return;
				}
				sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreate a Variable Spawner:"));
				sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create variable <default spawner> <name>   &fCreate using default as a base"));
				sender.sendMessage(CommonUtils.chat("&6&l>&fNote: &7Spawner name cant have space use _ instead."));
				return;
			}
		}
		sendHelp(sender);
	}
	@EventHandler
	public void onAsyncChat(AsyncPlayerChatEvent event) {
		if(!variableBuilding.containsKey(event.getPlayer()))
			return;
		event.setCancelled(true);
	}
	@EventHandler
	public void onChat(PlayerChatEvent event) {
		if(!variableBuilding.containsKey(event.getPlayer()))
			return;
		String message=event.getMessage();
		Player player=event.getPlayer();
		SpawnerBuilder builder = variableBuilding.get(player);
		VariableBuilder variable = builder.getVariable();
		event.setCancelled(true);
		event.setMessage("");
		if(!player.hasMetadata("EpicSpawnerBuilding")) {
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cUnable to create a variable spawner. Try Again."));
			variableBuilding.remove(player);
			return;
		}
		String tag=player.getMetadata("EpicSpawnerBuilding").get(0).asString();
		if(tag.equalsIgnoreCase("data")) {
			if(message.equalsIgnoreCase("end")) {
				SpawnerData spawner = builder.saveToFile().getSpawner();
				((EpicSpawnerStore)store).addSpawner(((EpicSpawnerData)spawner).getName(), spawner);
				player.removeMetadata("EpicSpawnerBuilding",plugin);
				variableBuilding.remove(player);
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fCreated new "+CommonUtils.format(SpawnerType.VARIABLE.name())+" spawner named "+CommonUtils.format(((EpicSpawnerData)spawner).getName())+"."));
				return;
			}
			if(message.split(",").length!=3) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter correct format"));
				player.sendMessage(CommonUtils.chat("&7-&f<min>,<max>,<spawnername>"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cTry again."));
				return;
			}
			int min;
			int max;
			String spawnerName=message.split(",")[2].toLowerCase();
			try {
				min=Integer.valueOf(message.split(",")[0]);
				max=Integer.valueOf(message.split(",")[1]);
			} catch(NullPointerException | NumberFormatException e) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter a number for min & max."));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cTry again."));
				return;
			}
			if(!store.isSpawner(spawnerName)) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter a correct spawner name"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cTry again."));
				return;
			}
			try {
				variable.addData(new VariableTypes(min, max, store.getSpawner(spawnerName)));
			} catch(IllegalArgumentException e) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&c"+e.getMessage()));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cTry again."));
				return;
			}
			builder.setVariable(variable);
			variableBuilding.put(player, builder);
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&aData entered Sucessfully"));
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fType end to finish entry or keep adding more entries."));
			player.sendMessage(CommonUtils.chat("&7-&f<min>,<max>,<spawnername>"));
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fmin & max are ranges & spawnername is the spawner to apply."));
			return;
		}
		if(tag.equalsIgnoreCase(ChangeBy.TIME.name())) {
			int time=0;
			try {
				time=Integer.valueOf(message);
			}catch(NullPointerException |NumberFormatException e) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter a number."));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cTry again."));
				return;
			}
			variable.setTime(time);
			builder.setVariable(variable);
			variableBuilding.put(player, builder);
			player.setMetadata("EpicSpawnerBuilding", new FixedMetadataValue(plugin, "data"));
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fPlease enter variable conditions in the format:"));
			player.sendMessage(CommonUtils.chat("&7-&f<min>,<max>,<spawnername>"));
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fmin & max are ranges & spawnername is the spawner to apply."));
			return;
		}
		if(tag.equalsIgnoreCase(ChangeBy.PLACEHOLDER.name())) {
			try {
				variable.setPlaceholder(message);
			} catch (IllegalArgumentException e) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&c"+e.getMessage()));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cTry again."));
				return;
			}
			builder.setVariable(variable);
			variableBuilding.put(player, builder);
			player.setMetadata("EpicSpawnerBuilding", new FixedMetadataValue(plugin, "data"));
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fPlease enter variable conditions in the format:"));
			player.sendMessage(CommonUtils.chat("&7-&f<min>,<max>,<spawnername>"));
			player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fmin & max are ranges & spawnername is the spawner to apply."));
			return;
		}
		if(tag.equalsIgnoreCase("type")) {
			ChangeBy by;
			try {
				by=ChangeBy.valueOf(message.toUpperCase());
			}catch(Exception e) {
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlease enter a change type."));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&7-placeholder"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&7-time"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&7-spawn"));
				return;
			}
			variable.setChangeBy(by);
			builder.setVariable(variable);
			variableBuilding.put(player, builder);
			switch (by) {
			case PLACEHOLDER:
				player.setMetadata("EpicSpawnerBuilding", new FixedMetadataValue(plugin, by.name()));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fPlease enter the placeholder to use."));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cPlaceholder must return a numeric value."));
				break;
			case TIME:
				player.setMetadata("EpicSpawnerBuilding", new FixedMetadataValue(plugin, by.name()));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fPlease enter the time in seconds"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cEnter a number."));
				break;
			case SPAWN:
				player.setMetadata("EpicSpawnerBuilding", new FixedMetadataValue(plugin, "data"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fPlease enter variable conditions in the format:"));
				player.sendMessage(CommonUtils.chat("&7-&f<min>,<max>,<spawnername>"));
				player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fmin & max are ranges & spawnername is the spawner to apply."));
				break;
			default:
				break;
			}
			return;
		}
	}
	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&cCommand unsupported by console."));
		return;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		alias.add("new");
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if (length == 2) {
			List<String> result = new ArrayList<>();
			List<String> options = new ArrayList<String>();
			for(SpawnerType type:SpawnerType.values())
				options.add(type.name().toLowerCase());
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		if (length == 3) {
			List<String> result = new ArrayList<>();
			List<String> options = new ArrayList<String>();
			options.add("byhand");
			options.add("byname");
			options.add("bylook");
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		if (length == 4) {
			List<String> result = new ArrayList<>();
			List<String> options = new ArrayList<String>();
			for(EntityType entity:EntityType.values())
				if (entity.isSpawnable() && entity.isAlive())
					options.add(entity.name().toLowerCase());
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}

		return new ArrayList<String>();
	}

	private void sendHelp(Player player) {
		player.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fSpawners Creator usage :"));
		player.sendMessage(CommonUtils.chat("&6&l>&f/espawner create item   &7Create item spawners"));
		player.sendMessage(CommonUtils.chat("&6&l>&f/espawner create custom_mob   &7Create mob spawners"));
		player.sendMessage(CommonUtils.chat("&6&l>&f/espawner create block   &7Create block spawners"));
		player.sendMessage(CommonUtils.chat("&6&l>&f/espawner create variable   &fCreate variable spawners"));
	}
	private Entity getEntity(Player player) {
		BlockIterator bi=new BlockIterator(player,10);
		Block lastblock;
		while (bi.hasNext()) {
			lastblock=bi.next();
			for(Entity entity:lastblock.getWorld().getNearbyEntities(lastblock.getLocation(), 0.5, 0.5, 0.5))
			{
				if(entity instanceof LivingEntity &&!( entity instanceof Player))
					return entity;
			}
		}
		return null;
	}
}
