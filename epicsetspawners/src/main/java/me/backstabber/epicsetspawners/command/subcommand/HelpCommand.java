package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class HelpCommand extends SubCommands {

	private String name = "help";

	public HelpCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods) {
		super(plugin, store,nmsMethods);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(CommonUtils.chat("&cYou Dont Have Permission."));
			return;
		}
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fUsage of /espawner command"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner give   &7Give a spawner to a player."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner create   &7Create a new custom spawner."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner delete   &7Delete a custom spawner."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner config   &7Reload & manage config.yml."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner applydefault   &7Apply default values to other spawners."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock   &7Change already placed spawners."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner help   &7Display this page."));
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fUsage of /espawner command"));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner give   &7Give a spawner to a player."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner delete   &7Delete a custom spawner."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner config   &7Reload & manage config.yml."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner applydefault   &7Apply default values to other spawners."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner updateblock   &7Change already placed spawners."));
		sender.sendMessage(CommonUtils.chat("&6&l>&f/espawner help   &7Display this page."));
		return;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		alias.add("info");
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		
		return new ArrayList<String>();
	}
}
