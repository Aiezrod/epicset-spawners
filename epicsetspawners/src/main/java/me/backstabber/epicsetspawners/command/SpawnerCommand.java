package me.backstabber.epicsetspawners.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.subcommand.ApplyDefaultCommand;
import me.backstabber.epicsetspawners.command.subcommand.ConfigCommand;
import me.backstabber.epicsetspawners.command.subcommand.CreateCommand;
import me.backstabber.epicsetspawners.command.subcommand.DeleteCommand;
import me.backstabber.epicsetspawners.command.subcommand.GiveCommand;
import me.backstabber.epicsetspawners.command.subcommand.HelpCommand;
import me.backstabber.epicsetspawners.command.subcommand.UpdateBlockCommand;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class SpawnerCommand implements CommandExecutor, TabCompleter {

	private List<SubCommands> subCommands = new ArrayList<SubCommands>();

	public SpawnerCommand(EpicSetSpawners plugin, SpawnerStore store,NmsMethods nmsMethods,LocationStore locations,CreateCommand command) {
		subCommands.add(new GiveCommand(plugin, store,nmsMethods));
		subCommands.add(command);
		subCommands.add(new DeleteCommand(plugin, store, nmsMethods));
		subCommands.add(new ConfigCommand(plugin, store, nmsMethods));
		subCommands.add(new ApplyDefaultCommand(plugin, store, nmsMethods));
		subCommands.add(new UpdateBlockCommand(plugin, store, nmsMethods, locations));
		subCommands.add(new HelpCommand(plugin, store, nmsMethods));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] sub) {

		if (sub.length == 0) {
			sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fIncorrect command. Type /espawner help."));
			return true;
		}
		for (SubCommands subs : subCommands)
			if (sub[0].equalsIgnoreCase(subs.getName()) || subs.getAliases().contains(sub[0].toLowerCase())) {
				if (sender instanceof Player)
					subs.onCommandByPlayer((Player) sender, sub);
				else
					subs.onCommandByConsole(sender, sub);
				return true;
			}
		sender.sendMessage(CommonUtils.chat("&6&lEpicSet-Spawners &7&l>&fIncorrect command. Type /espawner help."));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] sub) {
		if (sub.length == 1) {
			List<String> subs = new ArrayList<String>();
			List<String> result = new ArrayList<>();
			for (SubCommands command : subCommands) {
				subs.addAll(command.getAliases());
			}
			StringUtil.copyPartialMatches(sub[0], subs, result);
			Collections.sort(result);
			return result;
		} else if (sub.length > 1) {
			for (SubCommands command : subCommands) {
				if (command.getAliases().contains(sub[0].toLowerCase())) {
					return command.getCompletor(sub.length, sub[sub.length - 1]);
				}
			}
		}
		return new ArrayList<>();
	}

}
