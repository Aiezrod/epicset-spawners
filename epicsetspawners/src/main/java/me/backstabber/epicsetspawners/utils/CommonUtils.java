package me.backstabber.epicsetspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;
import net.milkbowl.vault.economy.Economy;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.*;
import java.util.Map.Entry;

public class CommonUtils {
	public static String getYear() {
		return String.valueOf(Year.now().getValue());
	}

	public static String getMonth() {
		Calendar cal = Calendar.getInstance();
		return (new SimpleDateFormat("MMM").format(cal.getTime()));
	}

	// function to check if a yml file isnt corrupted
	public static boolean checkConfig(File f) {
		YamlConfiguration yamlConfiguration = new YamlConfiguration();
		try {
			yamlConfiguration.load(f);
			return true;
		} catch (IOException e) {
			return false;
		} catch (InvalidConfigurationException e) {
			return false;
		}
	}

	// function to get rgb color from color name (cause MC colors are shit :D)
	public static Color getColorFromName(String name) {
		HashMap<String, Color> codes = new HashMap<>();
		codes.put("light_blue", Color.fromRGB(30, 144, 255));
		codes.put("black", Color.fromRGB(105, 105, 105));
		codes.put("cyan", Color.fromRGB(135, 206, 235));
		codes.put("magenta", Color.fromRGB(255, 0, 255));
		codes.put("gray", Color.fromRGB(211, 211, 211));
		codes.put("green", Color.fromRGB(34, 139, 34));
		codes.put("lime", Color.fromRGB(50, 205, 50));
		codes.put("brown", Color.fromRGB(139, 69, 19));
		codes.put("blue", Color.fromRGB(75, 0, 130));
		codes.put("orange", Color.fromRGB(255, 165, 0));
		codes.put("purple", Color.fromRGB(128, 0, 128));
		codes.put("red", Color.RED);
		codes.put("white", Color.fromRGB(255, 255, 255));
		codes.put("yellow", Color.fromRGB(255, 215, 0));
		codes.put("light_gray", Color.fromRGB(47, 79, 79));
		name = name.toLowerCase();
		name = name.replace(" ", "_");
		if (codes.containsKey(name))
			return codes.get(name);
		else
			return Color.WHITE;
	}

	// returns the server version as an double (easier to check for higher versions)
	public static double getServerVersion() {
		String version = Bukkit.getServer().getBukkitVersion().substring(2, 5);
		return Double.valueOf(version);
	}

	// formats the time into dd days HH hours mm min
	// or HH hours mm min ss sec
	// or mm min ss sec
	// depending on the time given
	public static String getTimeFormat(int seconds) {
		String total = "";
		if (seconds >= 86400) // atleast a day
		{
			int day = (int) toDays(seconds);
			int hour = (int) toHours(seconds);
			int min = (int) toMin(seconds);
			total = day + " days " + hour + " hours " + min + " min";
		} else if (seconds >= 3600) // atleast an hour
		{
			int hour = (int) toHours(seconds);
			int min = (int) toMin(seconds);
			int sec = toSec(seconds);
			total = hour + " hours " + min + " min " + sec + " sec";
		} else // a min or less
		{
			int min = (int) toMin(seconds);
			int sec = toSec(seconds);
			total = min + " min " + sec + " sec";
		}

		return total;
	}

	// extra functions for above logic
	private static int toSec(double total) {
		double temp = ((total / 86400.0D - toDays(total)) * 24.0D - toHours(total)) * 60 - toMin(total);
		return (int) Math.floor(temp * 60);
	}

	private static double toMin(double total) {
		double temp = (total / 86400.0D - toDays(total)) * 24.0D - toHours(total);
		return Math.floor(temp * 60.0D);
	}

	private static double toHours(double total) {
		double temp = total / 86400.0D - toDays(total);
		return Math.floor(temp * 24.0D);
	}

	private static double toDays(double total) {
		return Math.floor(total / 86400.0D);
	}

	// gets player's total experience (stolen from essentials :D)
	public static float getExperience(Player player) {
		int exp = Math.round(getExpAtLevel(player.getLevel()) * player.getExp());
		int currentLevel = player.getLevel();

		while (currentLevel > 0) {
			currentLevel--;
			exp += getExpAtLevel(currentLevel);
		}
		if (exp < 0) {
			exp = Integer.MAX_VALUE;
		}
		return exp;
	}

	public static void takeExperience(Player player, int xpCost) {
		setTotalExperience(player, (int) (getExperience(player) - xpCost));
	}

	// sets player's Experience
	public static void setTotalExperience(Player player, int exp) {
		player.setExp(0.0F);
		player.setLevel(0);
		player.setTotalExperience(0);

		int amount = exp;
		while (amount > 0) {
			int expToLevel = getExpAtLevel(player.getLevel());
			amount -= expToLevel;
			if (amount >= 0) {

				player.giveExp(expToLevel);
				continue;
			}
			amount += expToLevel;
			player.giveExp(amount);
			amount = 0;
		}
	}

	// extra fuction for above logic
	private static int getExpAtLevel(int level) {
		if (level <= 15) {
			return 2 * level + 7;
		}
		if (level >= 16 && level <= 30) {
			return 5 * level - 38;
		}
		return 9 * level - 158;
	}

	// returns item's name from itemstack
	public static String getItemName(ItemStack item) {
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
			return item.getItemMeta().getDisplayName();
		else {
			String name = "";
			for (String s : item.getType().name().split("_"))
				name = name + s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase() + " ";
			return name.substring(0, name.length() - 1);
		}
	}

	public static List<String> getItemLore(ItemStack item) {
		if (item.hasItemMeta() && item.getItemMeta().hasLore())
			return item.getItemMeta().getLore();
		return new ArrayList<String>();
	}

	// returns a string with prefix format(used to display bigger values)
	public static String getPrefixFormatted(long value) {
		// Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
		if (value == Long.MIN_VALUE)
			return getPrefixFormatted(Long.MIN_VALUE + 1);
		if (value < 0)
			return "-" + getPrefixFormatted(-value);
		if (value < 1000)
			return Long.toString(value); // deal with easy case

		Entry<Long, String> e = suffixes.floorEntry(value);
		Long divideBy = e.getKey();
		String suffix = e.getValue();

		long truncated = value / (divideBy / 10); // the number part of the output times 10
		boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
		return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
	}

	// map for upper function
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static final NavigableMap<Long, String> suffixes = new TreeMap();
	static {
		suffixes.put(1_000L, "k");
		suffixes.put(1_000_000L, "M");
		suffixes.put(1_000_000_000L, "G");
		suffixes.put(1_000_000_000_000L, "T");
		suffixes.put(1_000_000_000_000_000L, "P");
		suffixes.put(1_000_000_000_000_000_000L, "E");
	}

	// adds decimals to a number (commonly used for currency)
	public static String getDecimalFormatted(long amount) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
		formatter.setGroupingUsed(true);
		String formatted = formatter.format(amount).replace("$", "");
		return formatted.substring(0, formatted.length() - 3);
	}

	// gives item to player (drops item on player if his inv is full)
	public static void giveItem(Player player, ItemStack item) {
		if (item == null)
			return;
		if (player.getInventory().firstEmpty() < 0)
			player.getWorld().dropItem(player.getLocation(), item);
		else
			player.getInventory().addItem(item);
	}

	// gets an itemstack from an existing node in a yml file
	// if u save itemstack at ingame-item on node that is used
	// else nodes type,name&lore are used
	// this needs the following utilities to function properly
	// Umaterials (used for itemstack support from 1.8 to 1.15)
	// SkullCreator (used to generate custom skulls from online ids)
	public static ItemStack getItemFromNode(ConfigurationSection node) {
		if (node.isSet("ingame-item"))
			return node.getItemStack("ingame-item");
		else if (node.isSet("type"))
			return getCustomItem(node.getString("type"), node.getString("name"), node.getStringList("lore"));
		return null;
	}

	// extra function for above login (u can use this seperately if u want)
	@SuppressWarnings("deprecation")
	public static ItemStack getCustomItem(String type, String name, List<String> lore) {
		boolean glow = false;
		if (type.startsWith("<glow>")) {
			glow = true;
			type = type.replace("<glow>", "");
		}
		ItemStack item = EpicMaterials.valueOf(UMaterials.AIR).getItemStack();
		if (type.startsWith("<skull>")) {
			String sname = type.replace("<skull>", "");
			item = EpicMaterials.valueOf(UMaterials.PLAYER_HEAD_ITEM).getItemStack();
			SkullMeta sm = (SkullMeta) item.getItemMeta();
			sm.setOwner(sname);
			item.setItemMeta(sm);
		}
		else
			item = EpicMaterials.valueOf(type.toUpperCase()).getItemStack();
		ItemMeta im = item.getItemMeta();
		if (name != null) {
			im.setDisplayName(CommonUtils.chat(name));
		}
		if (lore != null && lore.size() > 0) {
			ArrayList<String> l = new ArrayList<String>();
			for (String s : lore)
				l.add(CommonUtils.chat(s));
			im.setLore(l);
		}
		item.setItemMeta(im);
		if (glow) {
			item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 4341);
			ItemMeta ime = item.getItemMeta();
			ime.addItemFlags(new ItemFlag[] { ItemFlag.HIDE_ENCHANTS });
			item.setItemMeta(ime);
		}
		return item;

	}

	// used to translate & to color
	public static String chat(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	// used to check chance of something
	public static boolean getChance(int max, int chance) {
		Random r = new Random();
		int d = r.nextInt(max) + 1;
		if (d <= chance)
			return true;
		return false;
	}
	// these use NBTEditor to function
	// I use them to store data inside items
	// its alot better to use nbt then name or lore

	// used to unregister any command from any plugin (i use this if my cmd conflict
	// with any other plugin)
	@SuppressWarnings("unchecked")
	public static void unRegisterCommand(String cmd, JavaPlugin plugin) {
		try {
			PluginCommand command = (PluginCommand) plugin.getCommand(cmd);
			Object result = getPrivateField(Bukkit.getPluginManager(), "commandMap");
			CommandMap commandMap = (CommandMap) result;
			HashMap<String, Command> knownCommands = (HashMap<String, Command>) getPrivateField(commandMap,
					"knownCommands");
			knownCommands.remove(command.getName());
			for (String alias : command.getAliases()) {
				if (knownCommands.containsKey(alias)
						&& knownCommands.get(alias).toString().contains(plugin.getName())) {
					knownCommands.remove(alias);
				}
			}
		} catch (Exception e) {

		}
	}

	// get head of any player
	public static ItemStack getSkull(Player player) {
		return getSkull(player.getName());
	}

	@SuppressWarnings("deprecation")
	public static ItemStack getSkull(String player) {
		ItemStack skull = EpicMaterials.valueOf(UMaterials.PLAYER_HEAD_ITEM).getItemStack();
		SkullMeta sm = (SkullMeta) skull.getItemMeta();
		sm.setOwner(player);
		sm.setDisplayName(player + "'s head");
		skull.setItemMeta(sm);
		return skull;
	}

	// used to evaluate arithematic expressions & get a double value (this is too
	// usefull)
	// expressions like (2*100)/50 etc
	public static double evaluateString(String str) {
		return new Object() {
			int pos = -1, ch;

			void nextChar() {
				ch = (++pos < str.length()) ? str.charAt(pos) : -1;
			}

			boolean eat(int charToEat) {
				while (ch == ' ')
					nextChar();
				if (ch == charToEat) {
					nextChar();
					return true;
				}
				return false;
			}

			double parse() {
				nextChar();
				double x = parseExpression();
				if (pos < str.length())
					throw new RuntimeException("Unexpected: " + (char) ch);
				return x;
			}

			// Grammar:
			// expression = term | expression `+` term | expression `-` term
			// term = factor | term `*` factor | term `/` factor
			// factor = `+` factor | `-` factor | `(` expression `)`
			// | number | functionName factor | factor `^` factor

			double parseExpression() {
				double x = parseTerm();
				for (;;) {
					if (eat('+'))
						x += parseTerm(); // addition
					else if (eat('-'))
						x -= parseTerm(); // subtraction
					else
						return x;
				}
			}

			double parseTerm() {
				double x = parseFactor();
				for (;;) {
					if (eat('*'))
						x *= parseFactor(); // multiplication
					else if (eat('/'))
						x /= parseFactor(); // division
					else
						return x;
				}
			}

			double parseFactor() {
				if (eat('+'))
					return parseFactor(); // unary plus
				if (eat('-'))
					return -parseFactor(); // unary minus

				double x;
				int startPos = this.pos;
				if (eat('(')) { // parentheses
					x = parseExpression();
					eat(')');
				} else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
					while ((ch >= '0' && ch <= '9') || ch == '.')
						nextChar();
					x = Double.parseDouble(str.substring(startPos, this.pos));
				} else if (ch >= 'a' && ch <= 'z') { // functions
					while (ch >= 'a' && ch <= 'z')
						nextChar();
					String func = str.substring(startPos, this.pos);
					x = parseFactor();
					if (func.equals("sqrt"))
						x = Math.sqrt(x);
					else if (func.equals("sin"))
						x = Math.sin(Math.toRadians(x));
					else if (func.equals("cos"))
						x = Math.cos(Math.toRadians(x));
					else if (func.equals("tan"))
						x = Math.tan(Math.toRadians(x));
					else
						throw new RuntimeException("Unknown function: " + func);
				} else {
					throw new RuntimeException("Unexpected: " + (char) ch);
				}

				if (eat('^'))
					x = Math.pow(x, parseFactor()); // exponentiation

				return x;
			}
		}.parse();
	}

	private static Object getPrivateField(Object object, String field)
			throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Class<?> clazz = object.getClass();
		Field objectField = clazz.getDeclaredField(field);
		objectField.setAccessible(true);
		return objectField.get(object);
	}

	public static double getBalance(Player player) {
		if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
			RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager()
					.getRegistration(Economy.class);
			if (rsp != null) {
				return rsp.getProvider().getBalance(player);
			}
		}
		return 0;
	}

	public static void takeBalance(Player player, int moneyCost) {
		if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
			RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager()
					.getRegistration(Economy.class);
			if (rsp != null) {
				rsp.getProvider().withdrawPlayer(player, moneyCost);
				return;
			}
		}
	}

	public static String format(String name) {
		String newName = "";
		for (String s : name.split("_"))
			newName = newName + s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase() + " ";
		return newName.substring(0, newName.length() - 1);
	}

	public static Location getBlockLocation(String locationString) {
		String[] split = locationString.split(";");
		return new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]),
				Integer.valueOf(split[3]));
	}

	public static String getBlockString(Location location) {
		return location.getWorld().getName() + ";" + location.getBlockX() + ";" + location.getBlockY() + ";"
				+ location.getBlockZ();
	}
}
