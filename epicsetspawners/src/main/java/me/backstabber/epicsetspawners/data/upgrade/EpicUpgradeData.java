package me.backstabber.epicsetspawners.data.upgrade;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.builder.validate.SpawnerPaths;
import me.backstabber.epicsetspawners.api.builder.validate.UpgradePaths;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.SpawnerData.SpawnerParameters;
import me.backstabber.epicsetspawners.api.data.UpgradeData;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;
import me.backstabber.epicsetspawners.utils.nbt.SafeNBT;

public class EpicUpgradeData implements UpgradeData {
	private EpicSetSpawners plugin;
	private SpawnerData spawner;
	private FileConfiguration parent;
	
	private boolean enabled;
	private int currentLevel;
	private int maxLevel;
	private Map<Integer, UpgradeStore> upgrades = new HashMap<Integer, UpgradeStore>();
	/*
	 * These are the default parameters. Also used for level 1. Can be changed with
	 * changeParameters() method
	 */
	private int minDelay=200;
	private int maxDelay=500;
	private int spawnCount=5;
	public EpicUpgradeData(SpawnerData spawner,FileConfiguration parent,EpicSetSpawners plugin) {
		this.parent=parent;
		this.spawner=spawner;
		this.plugin=plugin;
		init();
	}
	@Override
	public boolean isUpgradable() {
		if(spawner.getType().equals(SpawnerType.VARIABLE))
			return false;
		return this.enabled;
	}

	@Override
	public boolean isMaxed() {
		return this.currentLevel >= this.maxLevel;
	}

	@Override
	public int getLevel() {
		return this.currentLevel;
	}

	@Override
	public int getMaxLevel() {
		return this.maxLevel;
	}

	@Override
	public Map<DataType, Integer> getLevelData() {
		if (upgrades.containsKey(currentLevel)) {
			return upgrades.get(currentLevel).getData();
		} else {
			Map<DataType, Integer> data = new HashMap<>();
			data.put(DataType.MAX_DELAY, maxDelay);
			data.put(DataType.MIN_DELAY, minDelay);
			data.put(DataType.MOB_COUNT, spawnCount);
			return data;
		}
	}

	@Override
	public Map<DataType, Integer> getLevelData(int level) {
		if (upgrades.containsKey(level)) {
			return upgrades.get(level).getData();
		} else {
			Map<DataType, Integer> data = new HashMap<>();
			data.put(DataType.MAX_DELAY, maxDelay);
			data.put(DataType.MIN_DELAY, minDelay);
			data.put(DataType.MOB_COUNT, spawnCount);
			return data;
		}
	}

	public boolean canUpgrade(Player player) {
		if (!enabled || isMaxed())
			return false;
		return upgrades.get(currentLevel + 1).canUpgrade(player);
	}

	public boolean upgrade(Player player) {
		if (!canUpgrade(player))
			return false;
		if(!canUpgradeSpawner(player))
			return false;
		this.currentLevel++;
		this.upgrades.get(currentLevel).upgrade(player);
		EpicSetSpawners plugin = EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		String msg=plugin.getSettings().getFile().getString("message.upgrade-success");
		msg=msg.replace("%level%", currentLevel+"");
		msg=((EpicSpawnerData)spawner).replacePlaceholders(msg);
		player.sendMessage(CommonUtils.chat(msg));
		return true;
	}
	@Override
	public void changeParameter(DataType parameter, int data) {
		switch (parameter) {
		case MAX_DELAY:
			this.maxDelay = data;
			break;

		case MIN_DELAY:
			this.minDelay = data;
			break;

		case MOB_COUNT:
			this.spawnCount = data;
			break;

		default:
			break;
		}
	}

	public JsonObject getJson() {

		JsonObject json = new JsonObject();
		json.addProperty("enabled", enabled);
		json.addProperty("current-level", this.currentLevel);
		json.addProperty("min-delay", this.minDelay);
		json.addProperty("max-delay", this.maxDelay);
		json.addProperty("spawn-count", this.spawnCount);
		return json;
	}

	public void addData(JsonObject json) {
		this.enabled = json.get("enabled").getAsBoolean();
		this.currentLevel = json.get("current-level").getAsInt();
		this.minDelay = json.get("min-delay").getAsInt();
		this.maxDelay = json.get("max-delay").getAsInt();
		this.spawnCount = json.get("spawn-count").getAsInt();

	}

	public void init() {

		this.enabled = parent.getBoolean(UpgradePaths.ENABLED.getPath());
		if(parent.isSet(SpawnerPaths.SETTINGS_MIN_DELAY.getPath()))
			this.minDelay = parent.getInt(SpawnerPaths.SETTINGS_MIN_DELAY.getPath());
		if(parent.isSet(SpawnerPaths.SETTINGS_MAX_DELAY.getPath()))
			this.maxDelay = parent.getInt(SpawnerPaths.SETTINGS_MAX_DELAY.getPath());
		if(parent.isSet(SpawnerPaths.SETTINGS_SPAWN_COUNT.getPath()))
			this.spawnCount = parent.getInt(SpawnerPaths.SETTINGS_SPAWN_COUNT.getPath());
		if (enabled) {
			for (int index = 2; parent.isSet(UpgradePaths.LEVELS.getPath() + "." + index); index++) {
				this.maxLevel = index;
				UpgradeStore store = new UpgradeStore(spawner);
				if (store.setup(index, parent)) {
					this.upgrades.put(index, store);
				}
			}
			this.currentLevel = 1;
		}
	}

	public String replacePlaceholders(String s) {
		if (!isUpgradable()) {
			s = s.replace("%level%", "1");
			s = s.replace("%mindelay%", CommonUtils.getTimeFormat(minDelay/20));
			s = s.replace("%maxdelay%", CommonUtils.getTimeFormat(maxDelay/20));
			s = s.replace("%spawncount%", spawnCount + "");
			s = s.replace("%nextlevel%", "none");
			s = s.replace("%money%", "0");
			s = s.replace("%exp%", "0");
		} else {
			s = s.replace("%level%", currentLevel + "");
			if (currentLevel < 2) {
				s = s.replace("%mindelay%", CommonUtils.getTimeFormat(minDelay/20));
				s = s.replace("%maxdelay%", CommonUtils.getTimeFormat(maxDelay/20));
				s = s.replace("%spawncount%", spawnCount + "");
			} else {
				UpgradeStore store = upgrades.get(currentLevel);
				Map<DataType, Integer> data = store.getData();
				s = s.replace("%mindelay%", CommonUtils.getTimeFormat(data.get(DataType.MIN_DELAY)/20));
				s = s.replace("%maxdelay%", CommonUtils.getTimeFormat(data.get(DataType.MAX_DELAY)/20));
				s = s.replace("%spawncount%", CommonUtils.getDecimalFormatted(data.get(DataType.MOB_COUNT)));
			}
			if (isMaxed()) {
				s = s.replace("%nextlevel%", "none");
				s = s.replace("%money%", "0");
				s = s.replace("%exp%", "0");
			} else {
				s = s.replace("%nextlevel%", (currentLevel + 1) + "");
				UpgradeStore nextStore = upgrades.get(currentLevel + 1);
				s = nextStore.getCosts().replaceCosts(s);
			}
		}
		return s;
	}
	private boolean canUpgradeSpawner(Player player) {
		if(!plugin.getSettings().getFile().getBoolean("default-values.upgrade.requires-spawners"))
			return true;
		int amount=0;
		for(ItemStack item:player.getInventory().getContents()) {
			if(item==null||item.getType().equals(UMaterials.AIR.getMaterial()))
				continue;
			SafeNBT nbt = SafeNBT.get(item);
			String tag = nbt.getString("EpicSpawner");
			if (tag == null || tag.equals("")) 
				continue;
			JsonParser parser=new JsonParser();
			JsonObject json=(JsonObject) parser.parse(tag);
			if(json.get("EpicSetSpawner")!=null) {
				SpawnerType type=SpawnerType.valueOf(json.get("Type").getAsString().toUpperCase());
				String name=json.get("Name").getAsString();
				try {
				SpawnerData newSpawner=SpawnerBuilder.load(type, name).getSpawner(json);
				if(newSpawner.isSimilar(spawner)&&newSpawner.getUpgradeData().getLevel()==currentLevel) {
					int stacked=(int) newSpawner.getParameter(SpawnerParameters.AMOUNT);
					amount+=(item.getAmount()*stacked);
				}
				} catch(IllegalArgumentException e) {
				}
				
			}
		}
		if(amount>=2) {
			amount=2;
			for(int i=0;i<36;i++) {
				ItemStack item=player.getInventory().getItem(i);
				if(item==null||item.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
					continue;
				SafeNBT nbt = SafeNBT.get(item);
				String tag = nbt.getString("EpicSpawner");
				if (tag == null || tag.equals("")) 
					continue;
				JsonParser parser=new JsonParser();
				JsonObject json=(JsonObject) parser.parse(tag);
				if(json.get("EpicSetSpawner")!=null) {
					SpawnerType type=SpawnerType.valueOf(json.get("Type").getAsString().toUpperCase());
					String name=json.get("Name").getAsString();
					try {
					SpawnerData newSpawner=SpawnerBuilder.load(type, name).getSpawner(json);
					if(newSpawner.isSimilar(spawner)&&newSpawner.getUpgradeData().getLevel()==currentLevel) {
						int stacked=(int) newSpawner.getParameter(SpawnerParameters.AMOUNT);
						int totalAmount=item.getAmount()*stacked;
						if(totalAmount==amount) {
							player.getInventory().setItem(i, EpicMaterials.valueOf(UMaterials.AIR).getItemStack());
							return true;
						}
						else if(totalAmount>amount) {
							int leftAmount=totalAmount-amount;
							newSpawner.setStack(leftAmount);
							player.getInventory().setItem(i, newSpawner.getItemStack());
							return true;
						}
						else {
							amount=amount-totalAmount;
							player.getInventory().setItem(i, EpicMaterials.valueOf(UMaterials.AIR).getItemStack());
							continue;
						}
					}
					} catch(IllegalArgumentException e) {
					}
				}
			}
			return true;
		}
		String msg=plugin.getSettings().getFile().getString("message.upgrade-fail");
		String cost="&7Level "+currentLevel+" &e%entity%&7 Spawner";
		cost=((EpicSpawnerData)spawner).replacePlaceholders(cost);
		msg=msg.replace("%cost%", cost);
		player.sendMessage(CommonUtils.chat(msg));
		return false;
	}
}
