package me.backstabber.epicsetspawners.data.upgrade;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.backstabber.epicsetspawners.api.builder.validate.UpgradePaths;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.UpgradeData.DataType;

public class UpgradeStore {

	/*
	 * this object will act as an intermediate for upgrading for any upgrade to
	 * progress the cost & upgraded parameters will be stored & provided from this
	 * store
	 */
	private UpgradeCosts costs;
	private SpawnerData spawner;
	private int minDelay;
	private int maxDelay;
	private int spawnCount;
	public UpgradeStore(SpawnerData spawner) {
		this.spawner=spawner;
	}
	public boolean validate(int level, FileConfiguration file) {
		/*
		 * Upgrades are counted from level 2 & above Level 1 will be the default values.
		 */
		if (level < 2)
			return false;
		else {
			FileConfiguration config = file;
			if (config.isSet(UpgradePaths.LEVELS.getPath() + "." + level + ".cost.money")
					&& config.isSet(UpgradePaths.LEVELS.getPath() + "." + level + ".cost.exp")
					&& config.isSet(UpgradePaths.LEVELS.getPath() + "." + level + ".upgraded-settings.min-delay")
					&& config.isSet(UpgradePaths.LEVELS.getPath() + "." + level + ".upgraded-settings.max-delay")
					&& config.isSet(UpgradePaths.LEVELS.getPath() + "." + level + ".upgraded-settings.spawn-count"))
				return true;
			else
				return false;
		}
	}

	public boolean setup(int level, FileConfiguration file) {
		if (!validate(level, file))
			return false;
		FileConfiguration config = file;
		this.minDelay = config.getInt(UpgradePaths.LEVELS.getPath() + "." + level + ".upgraded-settings.min-delay");
		this.maxDelay = config.getInt(UpgradePaths.LEVELS.getPath() + "." + level + ".upgraded-settings.max-delay");
		this.spawnCount = config.getInt(UpgradePaths.LEVELS.getPath() + "." + level + ".upgraded-settings.spawn-count");
		this.costs = new UpgradeCosts(spawner, config.getInt(UpgradePaths.LEVELS.getPath() + "." + level + ".cost.money"),
				config.getInt(UpgradePaths.LEVELS.getPath() + "." + level + ".cost.exp"));
		return true;
	}

	public Map<DataType, Integer> getData() {
		Map<DataType, Integer> data = new HashMap<>();
		data.put(DataType.MAX_DELAY, maxDelay);
		data.put(DataType.MIN_DELAY, minDelay);
		data.put(DataType.MOB_COUNT, spawnCount);
		return data;
	}

	public boolean canUpgrade(Player player) {
		return costs.hasCosts(player);
	}

	public void upgrade(Player player) {
		costs.deductCosts(player);
	}

	public UpgradeCosts getCosts() {
		return costs;
	}

}
