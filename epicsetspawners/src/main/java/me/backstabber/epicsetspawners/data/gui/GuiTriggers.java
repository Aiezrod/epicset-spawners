package me.backstabber.epicsetspawners.data.gui;

public enum GuiTriggers {
	NONE, CLOSE_GUI, TOGGLE_SPAWNER, UPGRADE_SPAWNER;
}
