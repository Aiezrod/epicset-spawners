package me.backstabber.epicsetspawners.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.JsonObject;
import com.google.inject.Inject;

import me.backstabber.epicsetspawners.api.builder.validate.ChangeBy;
import me.backstabber.epicsetspawners.api.builder.validate.VariablePaths;
import me.backstabber.epicsetspawners.api.builder.validate.VariableTypes;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.VariableData;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.clip.placeholderapi.PlaceholderAPI;

public class EpicVariableData implements VariableData {
	@Inject
	private SpawnerStore store;
	
	private SpawnerData spawner;
	private FileConfiguration parentFile;
	
	private ChangeBy by;
	private OfflinePlayer player;
	
	private List<VariableTypes> values=new ArrayList<VariableTypes>();
	
	private Random random=new Random();
	private int timePassed=0;
	public EpicVariableData (SpawnerData spawner,FileConfiguration parent) {
		this.spawner=spawner;
		this.parentFile=parent;
	}
	@Override
	public OfflinePlayer getAttachedPlayer() {
		return player;
	}

	@Override
	public void setAttachedPlayer(Player player) {
		if(player!=null)
			this.player=player;
	}

	@Override
	public void updateBlock(Block block) {
		if(by.equals(ChangeBy.TIME)||by.equals(ChangeBy.SPAWN)) {
			VariableTypes type=getRandom();
			if(type!=null) {
				((EpicSpawnerData)type.getSpawner()).updateBlock(block);
			}
			else {
				((EpicSpawnerData)spawner).updateVariable(block);
			}
				
		}
		else if(this.player==null ||!this.player.isOnline()||this.values.size()==0) {
			((EpicSpawnerData)spawner).updateVariable(block);
		}
		else if(by.equals(ChangeBy.PLACEHOLDER)) {
			double value=Double.valueOf(PlaceholderAPI.setPlaceholders(player.getPlayer(), parentFile.getString(VariablePaths.PLACEHOLDER.getPath())));
			for(VariableTypes type:values) {
				if(type.isOverlap(value)) {
					((EpicSpawnerData)type.getSpawner()).updateBlock(block);
				}
			}
		}
		
	}
	@Override
	public void updateSpawn(Block block) {
		if(by.equals(ChangeBy.SPAWN)) {
			VariableTypes type=getRandom();
			if(type!=null) {
				((EpicSpawnerData)type.getSpawner()).updateBlock(block);
			}
			else {
				((EpicSpawnerData)spawner).updateVariable(block);
			}
				
		}
	}
	public void updateSecond(Block block) {
		this.timePassed++;
		if(this.by.equals(ChangeBy.TIME)&&this.timePassed>=parentFile.getInt(VariablePaths.TIME.getPath())) {
			updateBlock(block);
			this.timePassed=0;
		}
		else if(this.by.equals(ChangeBy.PLACEHOLDER)&&this.timePassed>=60) {
			updateBlock(block);
			this.timePassed=0;
		}
	}
	public JsonObject getJson() {

		JsonObject json = new JsonObject();
		if(this.player!=null) {
			json.addProperty("attachedPlayer", player.getUniqueId().toString());
		}
		return json;
	}

	public void addData(JsonObject json) {
		if(json.has("attachedPlayer"))
			this.player=Bukkit.getOfflinePlayer(UUID.fromString(json.get("attachedPlayer").getAsString()));
	}

	public void init() {
		if(!parentFile.isSet(VariablePaths.CHANGE_BY.getPath()))
			throw new IllegalArgumentException("Not a variable spawner.");
		this.by=ChangeBy.valueOf(parentFile.getString(VariablePaths.CHANGE_BY.getPath()).toUpperCase());
		ConfigurationSection part = parentFile.getConfigurationSection(VariablePaths.VALUES.getPath());
		if(part!=null) {
			for(String s: part.getKeys(false)) {
				int min=part.getInt(s+".min");
				int max=part.getInt(s+".max");
				String spawner=part.getString(s+".spawner");
				if(store.isSpawner(spawner))
					this.values.add(new VariableTypes(min, max, store.getSpawner(spawner)));
			}
		}
	}
	
	private VariableTypes getRandom() {
		Map<VariableTypes, Integer> weights=new HashMap<VariableTypes, Integer>();
		for(VariableTypes type:values) {
			weights.put(type, type.getMax());
		}
        if (weights == null || weights.isEmpty()) {
            return null;
        }
        double chance = random.nextDouble() * weights.values().stream().map(Number::doubleValue).reduce(0D, Double::sum);
        AtomicDouble needle = new AtomicDouble();
        return weights.entrySet().stream().filter((ent) -> {
            return needle.addAndGet(ent.getValue().doubleValue()) >= chance;
        }).findFirst().map(Map.Entry::getKey).orElse(null);
    }
}
