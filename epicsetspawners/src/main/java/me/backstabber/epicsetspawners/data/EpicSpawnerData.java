package me.backstabber.epicsetspawners.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.gson.JsonObject;
import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.builder.validate.SpawnerPaths;
import me.backstabber.epicsetspawners.api.data.GuiData;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.UpgradeData;
import me.backstabber.epicsetspawners.api.data.UpgradeData.DataType;
import me.backstabber.epicsetspawners.api.data.VariableData;
import me.backstabber.epicsetspawners.api.events.SpawnerBreakEvent;
import me.backstabber.epicsetspawners.api.events.SpawnerExplodeEvent;
import me.backstabber.epicsetspawners.api.events.SpawnerPlaceEvent;
import me.backstabber.epicsetspawners.api.events.SpawnerStackDownEvent;
import me.backstabber.epicsetspawners.api.events.SpawnerStackUpEvent;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.data.gui.EpicGuiData;
import me.backstabber.epicsetspawners.data.upgrade.EpicUpgradeData;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.stores.location.EpicLocationStore;
import me.backstabber.epicsetspawners.stores.location.StackedBlocks;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.UEnchants;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;
import me.backstabber.epicsetspawners.utils.nbt.SafeNBT;

public class EpicSpawnerData implements SpawnerData {

	@Inject
	private EpicSetSpawners plugin;
	@Inject
	private NmsMethods nmsMethods;
	@Inject
	private LocationStore locations;
	@Inject
	private StackedBlocks blocks;
	
	private FileConfiguration parent;
	private String name;
	
	private GuiData guiData;
	private UpgradeData upgradeData;
	private VariableData variableData;
	
	private boolean enabled=true;
	private int amount=1;
	
	private EntityType entity;
	protected String nbtTags;
	private SpawnerType type;
	
	private Random random=new Random();
	public EpicSpawnerData(FileConfiguration parent,String name) {
		this.name=name;
		this.parent=parent;
		this.entity=EntityType.valueOf(parent.getString(SpawnerPaths.SETTINGS_ENTITY.getPath()).toUpperCase());
		this.nbtTags=parent.getString(SpawnerPaths.SETTINGS_NBT_DATA.getPath());
		this.type=SpawnerType.valueOf(parent.getString(SpawnerPaths.SETTINGS_TYPE.getPath()).toUpperCase());
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		this.upgradeData=new EpicUpgradeData(this, parent,plugin);
		this.guiData=new EpicGuiData(this, parent);
		if(this.type.equals(SpawnerType.VARIABLE)) {
			this.variableData=new EpicVariableData(this, parent);
			plugin.injectMembers(this.variableData);
			((EpicVariableData)this.variableData).init();
		}
	}
	@Override
	public SpawnerData clone() {
		EpicSpawnerData data=new EpicSpawnerData(this.parent,this.name);
		plugin.injectMembers(data);
		return data;
	}
	@Override
	public ItemStack getItemStack() {
		String name = replacePlaceholders(parent.getString(SpawnerPaths.SPAWNER_ITEM_NAME.getPath()));
		List<String> lore = replacePlaceholders(
				parent.getStringList(SpawnerPaths.SPAWNER_ITEM_LORE.getPath()));
		ItemStack item = CommonUtils
				.getCustomItem(parent.getString(SpawnerPaths.SPAWNER_ITEM_TYPE.getPath()), name, lore);
		SafeNBT nbt = SafeNBT.get(item);
		nbt.setString("EpicSpawner", this.getJson().toString());
		item = nbt.apply(item);
		return item;
	}

	@Override
	public GuiData getGuiData() {
		return guiData;
	}

	@Override
	public UpgradeData getUpgradeData() {
		return upgradeData;
	}

	@Override
	public void applyToBlock(Block block, Player player) {
		// save this block in store
		locations.addSpawner(this, block);
		// check if this block is a spawner
		if (block.getState() instanceof CreatureSpawner) {
			// update this block using nbt tags
			if(this.type.equals(SpawnerType.VARIABLE)) {
				this.variableData.setAttachedPlayer(player);
				this.variableData.updateBlock(block);
			}
			else
				updateBlock(block);
		} else {
			// change block to a spawner
			block.setType(EpicMaterials.valueOf(UMaterials.SPAWNER).getMaterial());
			block.getState().update();
			// update this block using nbt tags
			new BukkitRunnable() {
				@Override
				public void run() {
					if(EpicSpawnerData.this.type.equals(SpawnerType.VARIABLE)) {
						EpicSpawnerData.this.variableData.setAttachedPlayer(player);
						EpicSpawnerData.this.variableData.updateBlock(block);
					}
					else
						updateBlock(block);
				}
			}.runTaskLater(plugin, 4);
		}
		// send message to player
		String message = this
				.replacePlaceholders(parent.getString(SpawnerPaths.MESSAGE_ONPLACE.getPath()));
		if (player != null)
			player.sendMessage(CommonUtils.chat(message));
		
	}

	@Override
	public void changeParameter(SpawnerParameters parameter, Object data) {
		switch (parameter) {
		case AMOUNT:
			this.amount = (int) data;
			break;

		case ENTITY:
			this.entity = (EntityType) data;
			break;

		case ENTITY_NBT:
			this.nbtTags = (String) data;
			break;

		default:
			break;
		}
	}

	@Override
	public Object getParameter(SpawnerParameters parameter) {
		switch (parameter) {
		case AMOUNT:
			return this.amount;

		case ENTITY:
			return this.entity;

		case ENTITY_NBT:
			return this.nbtTags;

		default:
			break;
		}
		return null;
	}

	@Override
	public SpawnerType getType() {
		return type;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled=enabled;
	}

	@Override
	public boolean isSimilar(SpawnerData data) {
		return (data instanceof EpicSpawnerData && ((EpicSpawnerData)data).name.equals(this.name) &&data.getUpgradeData().getLevel()==upgradeData.getLevel());
	}

	@Override
	public void setStack(int stack) {
		this.amount=stack;
	}
	
	
	public List<String> replacePlaceholders(List<String> stringList) {
		List<String> list = new ArrayList<String>();
		for (String s : stringList)
			list.add(replacePlaceholders(s));
		return list;
	}

	public String replacePlaceholders(String s) {
		s = s.replace("%name%", CommonUtils.format(name));
		s = s.replace("%filename%", CommonUtils.format(name));
		s = s.replace("%type%", CommonUtils.format(parent.getString(SpawnerPaths.SETTINGS_TYPE.getPath())));
		switch (type) {
		case BLOCK:
			s = s.replace("%entity%", CommonUtils.format(nbtTags));
			break;
		case ITEM:
			s = s.replace("%entity%", CommonUtils.format("Item"));
			break;

		case VANILLA:
		case CUSTOM_MOB:
		default:
			s = s.replace("%entity%", CommonUtils.format(parent.getString(SpawnerPaths.SETTINGS_ENTITY.getPath())));
			break;
		}
		s = s.replace("%stack%", CommonUtils.getDecimalFormatted(amount));
		if (enabled)
			s = s.replace("%state%", "&aenabled");
		else
			s = s.replace("%state%", "&cdisabled");
		s = ((EpicUpgradeData) upgradeData).replacePlaceholders(s);
		return s;
	}
	public String getName() {
		if(type.equals(SpawnerType.BLOCK)) {
			return CommonUtils.format(nbtTags);
		}
		return CommonUtils.format(name);
	}
	@SuppressWarnings("deprecation")
	public void placeBlock(BlockPlaceEvent placeEvent) {
		/*
		 * first check if placed against a spawner then check if both the spawners are
		 * the same by checking their clones if they are same then stack up
		 */
		Player player = placeEvent.getPlayer();
		ItemStack item = player.getInventory().getItemInHand();
		Block oldBlock = placeEvent.getBlockAgainst();
		if (locations.isStored(oldBlock)) {
			SpawnerData oldSpawner = locations.fetch(oldBlock);
			if (isSimilar(oldSpawner)) {
				// get previous stack size
				int stackSize = (int) oldSpawner.getParameter(SpawnerParameters.AMOUNT);
				// get additional stack size
				int itemStack = this.amount;
				if (placeEvent.getPlayer().isSneaking()) {
					itemStack = itemStack * item.getAmount();
					player.getInventory().setItemInHand(EpicMaterials.valueOf(UMaterials.AIR).getItemStack());
				} else {
					if (item.getAmount() > 1) {
						ItemStack temp = item.clone();
						temp.setAmount(temp.getAmount() - 1);
						player.getInventory().setItemInHand(temp);
					} else
						player.getInventory().setItemInHand(EpicMaterials.valueOf(UMaterials.AIR).getItemStack());
				}
				// set total stack
				int totalStack = stackSize + itemStack;
				oldSpawner.changeParameter(SpawnerParameters.AMOUNT, totalStack);
				// call stack up event
				SpawnerStackUpEvent stackUp = new SpawnerStackUpEvent(player, oldBlock, oldSpawner);
				Bukkit.getPluginManager().callEvent(stackUp);
				if (stackUp.isCancelled()) {
					// if event is cancelled the undo item taking & cancel spawner stacking
					player.getInventory().setItemInHand(item);
					placeEvent.setCancelled(true);
					return;
				}
				stackUp.getSpawner().applyToBlock(oldBlock, player);
				// if any spawner amount was changed by event give the player that amount back
				if ((int) stackUp.getSpawner().getParameter(SpawnerParameters.AMOUNT) < totalStack) {
					int leftOver = totalStack - (int) stackUp.getSpawner().getParameter(SpawnerParameters.AMOUNT);
					this.amount = leftOver;
					player.getInventory().addItem(this.getItemStack());
				}
				placeEvent.setCancelled(true);
				return;

			}
		}

		// place new spawner
		int stackSize = this.amount;
		if (player.isSneaking()) {
			stackSize = stackSize * item.getAmount();
			player.getInventory().setItemInHand(EpicMaterials.valueOf(UMaterials.AIR).getItemStack());
		}
		this.amount = stackSize;
		// call spawner place event
		SpawnerPlaceEvent spawnerPlaceEvent = new SpawnerPlaceEvent(player, placeEvent.getBlock(), this);
		Bukkit.getPluginManager().callEvent(spawnerPlaceEvent);
		if (spawnerPlaceEvent.isCancelled()) {
			// if event is cancelled the undo item taking & cancel block placing
			player.getInventory().setItemInHand(item);
			placeEvent.setCancelled(true);
			return;
		}
		spawnerPlaceEvent.getSpawner().applyToBlock(placeEvent.getBlock(), player);
		// if any spawner amount was changed by event give the player that amount back
		if ((int) spawnerPlaceEvent.getSpawner().getParameter(SpawnerParameters.AMOUNT) < stackSize) {
			int leftOver = stackSize - (int) spawnerPlaceEvent.getSpawner().getParameter(SpawnerParameters.AMOUNT);
			SpawnerData left = this.clone();
			left.changeParameter(SpawnerParameters.AMOUNT, leftOver);
			player.getInventory().addItem(left.getItemStack());
		}
		return;

	}

	public void breakBlock(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		event.setCancelled(true);
		if (amount == 1 || player.isSneaking()) {
			// break all the spawners
			ItemStack spawner = this.getItemStack();
			// test if there is any drop
			// check tool
			if (!(checkTool(player) && checkPerms(player)))
				spawner = EpicMaterials.valueOf(UMaterials.AIR).getItemStack();
			// call break event
			SpawnerBreakEvent breakEvent = new SpawnerBreakEvent(player, block, spawner, this);
			Bukkit.getPluginManager().callEvent(breakEvent);
			if (breakEvent.isCancelled()) {
				return;
			}
			((EpicLocationStore) locations).removeBlock(block);
			// remove hologram
			Location standLocation = block.getLocation().add(0.5, -1, 0.5);
			for (Entity entity : standLocation.getWorld().getNearbyEntities(standLocation, 0.5, 0.5, 0.5))
				if (entity instanceof ArmorStand)
					entity.remove();
			// remove block
			block.setType(EpicMaterials.valueOf(UMaterials.AIR).getMaterial());
			// send break message
			String message = ((EpicSpawnerData) breakEvent.getSpawner())
					.replacePlaceholders(parent.getString(SpawnerPaths.MESSAGE_ONBREAK.getPath()));
			player.sendMessage(CommonUtils.chat(message));
			// give spawner to player
			ItemStack toGive = breakEvent.getDrop();
			if (toGive == null || toGive.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
				return;
			if (parent.getBoolean(SpawnerPaths.BREAKING_IN_INVENTORY.getPath()))
				CommonUtils.giveItem(player, toGive);
			else
				block.getWorld().dropItemNaturally(block.getLocation(), toGive);
			return;
		} else {
			int totalAmount = amount;
			this.amount = 1;
			ItemStack spawner = this.getItemStack();
			this.amount = totalAmount - 1;
			// test for silk touch
			if (!(checkPerms(player) && checkTool(player)))
				spawner = EpicMaterials.valueOf(UMaterials.AIR).getItemStack();
			// call stack down event
			SpawnerStackDownEvent downEvent = new SpawnerStackDownEvent(player, block, spawner, this);
			Bukkit.getPluginManager().callEvent(downEvent);
			if (downEvent.isCancelled()) {
				return;
			}
			// update the leftover spawners
			downEvent.getSpawner().applyToBlock(block, player);
			// give spawner to player
			ItemStack toGive = downEvent.getDrop();
			if (toGive == null || toGive.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
				return;
			if (parent.getBoolean(SpawnerPaths.BREAKING_IN_INVENTORY.getPath()))
				CommonUtils.giveItem(player, toGive);
			else
				block.getWorld().dropItemNaturally(block.getLocation(), toGive);
			return;
		}
	}

	public  void blockExplode(Block block) {
		((EpicLocationStore) locations).removeBlock(block);
		// remove hologram
		Location standLocation = block.getLocation().add(0.5, -1, 0.5);
		for (Entity entity : standLocation.getWorld().getNearbyEntities(standLocation, 0.5, 0.5, 0.5))
			if (entity instanceof ArmorStand)
				entity.remove();
		ItemStack spawner = this.getItemStack();
		// call explode event
		SpawnerExplodeEvent explodeEvent = new SpawnerExplodeEvent(block, spawner, this);
		Bukkit.getPluginManager().callEvent(explodeEvent);
		if (explodeEvent.isCancelled()) {
			return;
		}
		ItemStack toGive = explodeEvent.getDrop();
		if (toGive == null || toGive.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
			return;
		block.getWorld().dropItemNaturally(block.getLocation(), toGive);
	}
	public boolean canSpawn(Block spawner, Entity spawned) {
		Location loc=spawned.getLocation();
		if(type.equals(SpawnerType.VARIABLE)&&this.variableData!=null) {
			this.variableData.updateSpawn(spawner);
		}
		if(type.equals(SpawnerType.CUSTOM_MOB)) {
			spawned.setMetadata("EpicSpawnerEntityBypass", new FixedMetadataValue(plugin, true));
			return true;
		}
		if(type.equals(SpawnerType.BLOCK)) {
			Material blockType = EpicMaterials.valueOf(nbtTags.toUpperCase()).getMaterial();
			int existingBlocks = 0;
			for (int xDiff = -3; xDiff <= 3; xDiff++)
				for (int yDiff = -3; yDiff <= 3; yDiff++)
					for (int zDiff = -3; zDiff <= 3; zDiff++)
						if (spawner.getLocation().clone().add(xDiff, yDiff, zDiff).getBlock().getType().equals(blockType))
							existingBlocks++;
			if (existingBlocks >= 7)
				return false;
			loc = loc.add(0, 1, 0);
			if (loc.getBlock().getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial())) {
				loc.getBlock().setType(blockType);
				blocks.setBlock(loc.getBlock(), getAmount());
			}
			loc.getBlock().getState().update(true);
			return false;
		}
		return true;
	}
	public int getAmount() {
		Map<DataType, Integer> data = upgradeData.getLevelData();
		int mobAmount = this.amount * data.get(DataType.MOB_COUNT);
		return mobAmount;
	}
	public void save(Block block) {
		locations.addSpawner(this, block);
	}
	public JsonObject getJson() {
		JsonObject json=new JsonObject();
		json.addProperty("EpicSetSpawner", true);
		json.addProperty("Name", name);
		json.addProperty("Type", type.name());
		JsonObject data=new JsonObject();
		data.addProperty("amount", amount);
		data.addProperty("entity", entity.name());
		data.addProperty("nbt", nbtTags);
		json.add("SpawnerData", data);
		json.add("UpgradeData", ((EpicUpgradeData)upgradeData).getJson());
		if(this.type.equals(SpawnerType.VARIABLE)&&this.variableData!=null)
			json.add("VariableData", ((EpicVariableData)this.variableData).getJson());
		return json;
	}
	public void addData(JsonObject json) {
		if(json.get("SpawnerData")!=null&&json.get("SpawnerData").isJsonObject()) {
			JsonObject data=json.get("SpawnerData").getAsJsonObject();
			this.amount=data.get("amount").getAsInt();
			this.entity=EntityType.valueOf(data.get("entity").getAsString().toUpperCase());
			this.nbtTags=data.get("nbt").getAsString();
			((EpicUpgradeData)this.upgradeData).addData(json.get("UpgradeData").getAsJsonObject());
			if(this.type.equals(SpawnerType.VARIABLE)&&this.variableData!=null)
				((EpicVariableData)this.variableData).addData(json.get("VariableData").getAsJsonObject());
		}
	}
	public void addSecond(Block block) {
		if(this.type.equals(SpawnerType.VARIABLE)&&this.variableData!=null) {
			((EpicVariableData)this.variableData).updateSecond(block);
		}
	}
	void updateVariable(Block block) {
		if(!this.type.equals(SpawnerType.VARIABLE))
			return;
		block.breakNaturally();
		block.setType(EpicMaterials.valueOf(UMaterials.SPAWNER).getMaterial());
		Map<DataType, Integer> data = upgradeData.getLevelData();
		int initial=random.nextInt(data.get(DataType.MIN_DELAY)+data.get(DataType.MAX_DELAY))-data.get(DataType.MIN_DELAY);
		int mobAmount = this.amount * data.get(DataType.MOB_COUNT);
		if(this.entity.equals(EntityType.DROPPED_ITEM)) {
			try {
				EpicMaterials.valueOf(nbtTags.toUpperCase()).getItemStack();
				nmsMethods.addNBTTags(block, "Item",
						nmsMethods.getItemNbt(EpicMaterials.valueOf(nbtTags.toUpperCase()).getItemStack()), initial,
						data.get(DataType.MIN_DELAY), data.get(DataType.MAX_DELAY), 10, mobAmount);
			}catch (Exception e) {
				nmsMethods.addNBTTags(block, "Item", nbtTags, initial, data.get(DataType.MIN_DELAY),
						data.get(DataType.MAX_DELAY), 10, mobAmount);
			}
		}
		else {
			nmsMethods.addNBTTags(block, entity.name(), nbtTags, initial, data.get(DataType.MIN_DELAY),
					data.get(DataType.MAX_DELAY), 10, mobAmount);
		}
		// spawn hologram
		Location standLocation = block.getLocation().add(0.5, -1, 0.5);
		// remove any previous armor stand if exists
		for (Entity entity : standLocation.getWorld().getNearbyEntities(standLocation, 0.5, 0.5, 0.5))
			if (entity instanceof ArmorStand)
				entity.remove();
		// add armorstand with count
		ArmorStand stand = standLocation.getWorld().spawn(standLocation, ArmorStand.class);
		stand.setGravity(false);
		stand.setBasePlate(false);
		stand.setOp(true);
		stand.setVisible(false);
		String format = this
				.replacePlaceholders(parent.getString(SpawnerPaths.SPAWNER_STACKED_NAME.getPath()));
		stand.setCustomName(CommonUtils.chat(format));
		stand.setCustomNameVisible(true);
	}

	void updateBlock(Block block) {
		block.breakNaturally();
		block.setType(EpicMaterials.valueOf(UMaterials.SPAWNER).getMaterial());
		Map<DataType, Integer> data = upgradeData.getLevelData();
		int initial=random.nextInt(data.get(DataType.MIN_DELAY)+data.get(DataType.MAX_DELAY))-data.get(DataType.MIN_DELAY);
		int mobAmount = this.amount * data.get(DataType.MOB_COUNT);
		switch (type) {
		case VANILLA:
			nmsMethods.addNBTTags(block, this.entity.name(), null, initial, data.get(DataType.MIN_DELAY),
					data.get(DataType.MAX_DELAY), 10, mobAmount);
			break;
		case ITEM:
			nmsMethods.addNBTTags(block, "Item", nbtTags, initial, data.get(DataType.MIN_DELAY),
					data.get(DataType.MAX_DELAY), 10, mobAmount);
			break;
		case CUSTOM_MOB:
			nmsMethods.addNBTTags(block, entity.name(), nbtTags, initial, data.get(DataType.MIN_DELAY),
					data.get(DataType.MAX_DELAY), 10, mobAmount);
			break;
		case BLOCK:
			nmsMethods.addNBTTags(block, "Item",
					nmsMethods.getItemNbt(EpicMaterials.valueOf(nbtTags.toUpperCase()).getItemStack()), initial,
					data.get(DataType.MIN_DELAY), data.get(DataType.MAX_DELAY), 10, mobAmount);
			break;
		default:
			break;
		}
		// spawn hologram
		Location standLocation = block.getLocation().add(0.5, -1, 0.5);
		// remove any previous armor stand if exists
		for (Entity entity : standLocation.getWorld().getNearbyEntities(standLocation, 0.5, 0.5, 0.5))
			if (entity instanceof ArmorStand)
				entity.remove();
		// add armorstand with count
		ArmorStand stand = standLocation.getWorld().spawn(standLocation, ArmorStand.class);
		stand.setGravity(false);
		stand.setBasePlate(false);
		stand.setOp(true);
		stand.setVisible(false);
		String format = this
				.replacePlaceholders(parent.getString(SpawnerPaths.SPAWNER_STACKED_NAME.getPath()));
		stand.setCustomName(CommonUtils.chat(format));
		stand.setCustomNameVisible(true);
	}
	private boolean checkPerms(Player player) {

		if (parent.getBoolean(SpawnerPaths.BREAKING_PERMS_ENABLED.getPath())) {
			return player.hasPermission(parent.getString(SpawnerPaths.BREAKING_PERMS_PERM.getPath()));
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	private boolean checkTool(Player player) {

		if (parent.getBoolean(SpawnerPaths.BREAKING_TOOL_ENABLED.getPath())) {
			ItemStack item = player.getInventory().getItemInHand();
			if (item == null || item.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
				return false;
			String name = getToolName(item);
			for (String types : parent.getStringList(SpawnerPaths.BREAKING_TOOL_TYPES.getPath()))
				if (types.equalsIgnoreCase(name)) {
					List<String> enchants = new ArrayList<>();
					for (Enchantment enchant : item.getEnchantments().keySet())
						enchants.add(UEnchants.valueOf(enchant).name().toLowerCase());
					for (String type : parent
							.getStringList(SpawnerPaths.BREAKING_TOOL_ENCHANTS.getPath()))
						if (!type.replace(" ", "").equals("") && !enchants.contains(type.toLowerCase()))
							return false;
					return true;
				}
			return false;
		}
		return true;
	}

	private String getToolName(ItemStack itemInHand) {
		String type = itemInHand.getType().name();
		type = type.toLowerCase();
		return type.replace("_", "").replace("%chainmail%", "").replace("diamond", "").replace("wooden", "")
				.replace("wood", "").replace("iron", "").replace("golden", "").replace("gold", "");
	}
}
