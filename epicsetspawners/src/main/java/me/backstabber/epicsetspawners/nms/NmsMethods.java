package me.backstabber.epicsetspawners.nms;


import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.hooks.StackersHook;

public abstract class NmsMethods {
	protected StackersHook hook;

	public NmsMethods(StackersHook hook) {
		this.hook = hook;
	}
	public abstract String getItemNbt(ItemStack item);

	public abstract String getEntityNbt(Entity entity);
	public abstract void addNBTTags(Block block, String entityId, String spawnData, int delay, int minDelay,
			int maxDelay, int spawnRange, int amount);
}
