package me.backstabber.epicsetspawners.nms.impl;

import static me.backstabber.epicsetspawners.utils.ReflectionUtils.getRefClass;

import org.bukkit.Location;
import org.bukkit.block.Block;

import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.nms.LegacyMethods;
import me.backstabber.epicsetspawners.utils.ReflectionUtils.RefConstructor;
import me.backstabber.epicsetspawners.utils.ReflectionUtils.RefMethod;

public class Version1_12 extends LegacyMethods {
	public Version1_12(StackersHook hook) {
		super(hook);
	}

	private static RefConstructor BlockPosition = getRefClass("{nms}.BlockPosition").getConstructor(int.class,
			int.class, int.class);
	private static RefConstructor NBTTagCompound = getRefClass("{nms}.NBTTagCompound").getConstructor();
	private static RefMethod getTileEntity = getRefClass("{nms}.WorldServer").getMethod("getTileEntity",
			getRefClass("{nms}.BlockPosition").getRealClass());
	private static RefMethod getHandle = getRefClass("{cb}.CraftWorld").getMethod("getHandle");
	private static RefMethod save = getRefClass("{nms}.TileEntityMobSpawner").getMethod("save",
			getRefClass("{nms}.NBTTagCompound").getRealClass());
	private static RefMethod load = getRefClass("{nms}.TileEntityMobSpawner").getMethod("load",
			getRefClass("{nms}.NBTTagCompound").getRealClass());
	private static RefMethod parse = getRefClass("{nms}.MojangsonParser").getMethod("parse", String.class);
	private static RefMethod remove = getRefClass("{nms}.NBTTagCompound").getMethod("remove", String.class);
	private static RefMethod set = getRefClass("{nms}.NBTTagCompound").getMethod("set", String.class,
			getRefClass("{nms}.NBTBase").getRealClass());
	private static RefMethod setString = getRefClass("{nms}.NBTTagCompound").getMethod("setString", String.class,
			String.class);
	private static RefMethod setShort = getRefClass("{nms}.NBTTagCompound").getMethod("setShort", String.class,
			short.class);

	@Override
	public void addNBTTags(Block block, String entityId, String spawnData, int delay, int minDelay, int maxDelay,
			int spawnRange, int amount) {
		// set item spawner data
		Location location = block.getLocation();

		Object blockPos = BlockPosition.create(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		Object spawner = getTileEntity.of(getHandle.of(location.getWorld()).call()).call(blockPos);
		Object tag = NBTTagCompound.create();
		save.of(spawner).call(tag);
		// setting basic data using nbt tags
		if (hook.isEntityEnabled())
			setShort.of(tag).call("SpawnCount", (short) 1);
		else
			setShort.of(tag).call("SpawnCount", (short) amount);
		setShort.of(tag).call("Delay", (short) delay);
		setShort.of(tag).call("MaxSpawnDelay", (short) maxDelay);
		setShort.of(tag).call("MinSpawnDelay", (short) minDelay);
		setShort.of(tag).call("SpawnRange", (short) 5);
		setShort.of(tag).call("MaxNearbyEntities", (short) 5);
		setShort.of(tag).call("RequiredPlayerRange", (short) spawnRange);
		// set mob type & extra SpawnData
		Object spawnDataTag = NBTTagCompound.create();
		try {
			spawnDataTag = parse.call(spawnData);
		} catch (Exception e) {

		}
		Object itemTag = NBTTagCompound.create();
		if (entityId.equalsIgnoreCase("item")) {
			remove.of(tag).call("SpawnPotentials");
			setString.of(tag).call("EntityId", "item");
			setString.of(itemTag).call("id", "item");
			set.of(itemTag).call(entityId, spawnDataTag);
			set.of(tag).call("SpawnData", itemTag);
		} else {
			setString.of(spawnDataTag).call("id", "minecraft:" + entityId.toLowerCase() + "");
			remove.of(spawnDataTag).call("Pos");
			remove.of(spawnDataTag).call("UUID");
			set.of(tag).call("SpawnData", spawnDataTag);
			set.of(tag).call("SpawnPotentials", spawnDataTag);
		}
		load.of(spawner).call(tag);
		/*
		 * net.minecraft.server.v1_12_R1.BlockPosition blockPos=new
		 * net.minecraft.server.v1_12_R1.BlockPosition(location.getBlockX(),
		 * location.getBlockY(), location.getBlockZ()); TileEntityMobSpawner
		 * spawner=(TileEntityMobSpawner)
		 * ((CraftWorld)location.getWorld()).getHandle().getTileEntity(blockPos);
		 * net.minecraft.server.v1_12_R1.NBTTagCompound tag=new
		 * net.minecraft.server.v1_12_R1.NBTTagCompound(); spawner.save(tag);
		 * net.minecraft.server.v1_12_R1.NBTTagCompound spawnDataTag=new
		 * net.minecraft.server.v1_12_R1.NBTTagCompound(); try {
		 * spawnDataTag=MojangsonParser.parse(spawnData); }catch(Exception e) { return;
		 * } net.minecraft.server.v1_12_R1.NBTTagCompound itemTag=new
		 * net.minecraft.server.v1_12_R1.NBTTagCompound();
		 * if(entityId.equalsIgnoreCase("item")) { tag.remove("SpawnPotentials");
		 * tag.setString("EntityId", entityId); itemTag.setString("id", entityId);
		 * itemTag.set(entityId, spawnDataTag); tag.set("SpawnData", itemTag); } else {
		 * spawnDataTag.setString("id", "minecraft:"+entityId.toLowerCase()+"");
		 * spawnDataTag.remove("Pos"); tag.set("SpawnData", spawnDataTag);
		 * tag.set("SpawnPotentials", spawnDataTag); } spawner.load(tag);
		 */
	}

}
