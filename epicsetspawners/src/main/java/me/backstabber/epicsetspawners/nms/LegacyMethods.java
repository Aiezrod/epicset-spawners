package me.backstabber.epicsetspawners.nms;

import static me.backstabber.epicsetspawners.utils.ReflectionUtils.getRefClass;

import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.utils.ReflectionUtils.RefConstructor;
import me.backstabber.epicsetspawners.utils.ReflectionUtils.RefMethod;

public abstract class LegacyMethods extends NmsMethods {

	public LegacyMethods(StackersHook hook) {
		super(hook);
	}

	private static RefConstructor NBTTagCompound = getRefClass("{nms}.NBTTagCompound").getConstructor();
	private static RefMethod asNMSCopy = getRefClass("{cb}.inventory.CraftItemStack").getMethod("asNMSCopy",
			ItemStack.class);
	private static RefMethod saveItem = getRefClass("{nms}.ItemStack").getMethod("save",
			getRefClass("{nms}.NBTTagCompound").getRealClass());
	private static RefMethod tagToString = getRefClass("{nms}.NBTTagCompound").getMethod("toString");

	private static RefMethod getEntityHandle = getRefClass("{cb}.entity.CraftEntity").getMethod("getHandle");
	private static RefMethod c = getRefClass("{nms}.Entity").getMethod("c",
			getRefClass("{nms}.NBTTagCompound").getRealClass());
	@Override
	public String getItemNbt(ItemStack item) {
		Object nmsItem = asNMSCopy.call(item);
		Object tag = NBTTagCompound.create();
		saveItem.of(nmsItem).call(tag);
		return (String) tagToString.of(tag).call();

		/*
		 * net.minecraft.server.v1_14_R1.ItemStack
		 * nmsItem=CraftItemStack.asNMSCopy(item);
		 * net.minecraft.server.v1_14_R1.NBTTagCompound tag=new
		 * net.minecraft.server.v1_14_R1.NBTTagCompound(); nmsItem.save(tag); return
		 * tag.toString();
		 */
	}
	@Override
	public String getEntityNbt(Entity entity) {
		Object nmsEntity = getEntityHandle.of(entity).call();
		Object tag = NBTTagCompound.create();
		c.of(nmsEntity).call(tag);
		return (String) tagToString.of(tag).call();

		/*
		 * net.minecraft.server.v1_14_R1.Entity
		 * nmsEntity=((CraftEntity)entity).getHandle();
		 * net.minecraft.server.v1_14_R1.NBTTagCompound tag=new
		 * net.minecraft.server.v1_14_R1.NBTTagCompound(); nmsEntity.c(tag); return
		 * tag.toString();
		 */
	}
}
