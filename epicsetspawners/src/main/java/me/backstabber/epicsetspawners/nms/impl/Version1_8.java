package me.backstabber.epicsetspawners.nms.impl;

import static me.backstabber.epicsetspawners.utils.ReflectionUtils.getRefClass;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;

import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.nms.LegacyMethods;
import me.backstabber.epicsetspawners.utils.NBTEditor;
import me.backstabber.epicsetspawners.utils.ReflectionUtils.RefConstructor;
import me.backstabber.epicsetspawners.utils.ReflectionUtils.RefMethod;

public class Version1_8 extends LegacyMethods {
	public Version1_8(StackersHook hook) {
		super(hook);
	}

	private static RefConstructor BlockPosition = getRefClass("{nms}.BlockPosition").getConstructor(int.class,
			int.class, int.class);
	private static RefConstructor NBTTagCompound = getRefClass("{nms}.NBTTagCompound").getConstructor();
	private static RefMethod getTileEntity = getRefClass("{nms}.WorldServer").getMethod("getTileEntity",
			getRefClass("{nms}.BlockPosition").getRealClass());
	private static RefMethod getHandle = getRefClass("{cb}.CraftWorld").getMethod("getHandle");
	private static RefMethod save = getRefClass("{nms}.TileEntityMobSpawner").getMethod("b",
			getRefClass("{nms}.NBTTagCompound").getRealClass());
	private static RefMethod load = getRefClass("{nms}.TileEntityMobSpawner").getMethod("a",
			getRefClass("{nms}.NBTTagCompound").getRealClass());
	private static RefMethod parse = getRefClass("{nms}.MojangsonParser").getMethod("parse", String.class);
	private static RefMethod remove = getRefClass("{nms}.NBTTagCompound").getMethod("remove", String.class);
	private static RefMethod set = getRefClass("{nms}.NBTTagCompound").getMethod("set", String.class,
			getRefClass("{nms}.NBTBase").getRealClass());
	private static RefMethod setString = getRefClass("{nms}.NBTTagCompound").getMethod("setString", String.class,
			String.class);

	@Override
	public void addNBTTags(Block block, String entityId, String spawnData, int delay, int minDelay, int maxDelay,
		int spawnRange, int amount) {
		// check if spawnData is used or not
		if (spawnData == null || !entityId.equalsIgnoreCase("item")) // not
		{
			// use vanilla method to change mob
			CreatureSpawner spawner = (CreatureSpawner) block.getState();
			spawner.setSpawnedType(EntityType.valueOf(entityId));
			block.getState().update();
		}
		// set item spawner data
		Location location = block.getLocation();
		Object blockPos = BlockPosition.create(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		Object server = getHandle.of(location.getWorld()).call();
		Object spawner = getTileEntity.of(server).call(blockPos);
		Object tag = NBTTagCompound.create();
		// get nbtcompound from spawnData
		Object SpawnDataTag = NBTTagCompound.create();
		// set basic data
		if (hook.isEntityEnabled())
			NBTEditor.set(block, (short) 1, "SpawnCount");
		else
			NBTEditor.set(block, (short) amount, "SpawnCount");
		NBTEditor.set(block, (short) delay, "Delay");
		NBTEditor.set(block, (short) maxDelay, "MaxSpawnDelay");
		NBTEditor.set(block, (short) minDelay, "MinSpawnDelay");
		NBTEditor.set(block, (short) 5, "SpawnRange");
		NBTEditor.set(block, (short) 5, "MaxNearbyEntities");
		NBTEditor.set(block, (short) spawnRange, "RequiredPlayerRange");

		// set entity type & spawnData
		try {
			SpawnDataTag = parse.call(spawnData);
		} catch (Exception e) {

		}
		Object itemtag = NBTTagCompound.create();

		save.of(spawner).call(tag);
		if (entityId.equalsIgnoreCase("item")) {
			remove.of(tag).call("SpawnPotentials");
			setString.of(tag).call("EntityId", entityId);
			set.of(itemtag).call(entityId, SpawnDataTag);
			set.of(tag).call("SpawnData", itemtag);
			/*
			 * remove.of(tag).call("SpawnPotentials");
			 * setString.of(SpawnDataTag).call("id","minecraft:"+entityId.toLowerCase()+"");
			 * set.of(itemtag).call(entityId,SpawnDataTag);
			 * set.of(tag).call("SpawnData",itemtag);
			 */
		} else {
			setString.of(SpawnDataTag).call("id", "minecraft:" + entityId.toLowerCase() + "");
			remove.of(SpawnDataTag).call("Pos");
			set.of(tag).call("SpawnData", SpawnDataTag);
			set.of(tag).call("SpawnPotentials", SpawnDataTag);
		}
		load.of(spawner).call(tag);
	}
}
