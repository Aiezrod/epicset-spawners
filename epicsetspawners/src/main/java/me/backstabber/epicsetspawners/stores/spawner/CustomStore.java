package me.backstabber.epicsetspawners.stores.spawner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.spawner.NonVanillaStore;

public class CustomStore implements NonVanillaStore {

	private Map<String, SpawnerData> spawners = new HashMap<String, SpawnerData>();

	@Override
	public boolean isSpawner(String spawnerName) {

		return spawners.containsKey(spawnerName);
	}

	@Override
	public void removeSpawner(String spawnerName) {

		SpawnerData spawner = spawners.get(spawnerName);
		SpawnerType type = spawner.getType();
		type.getFile(spawnerName).getLoc().delete();
		spawners.remove(spawnerName);
	}

	@Override
	public SpawnerData getSpawner(String spawnerName) {
		if (isSpawner(spawnerName))
			return spawners.get(spawnerName);
		return null;
	}

	@Override
	public void addSpawner(String spawnerName, SpawnerData spawner) {

		spawners.put(spawnerName, spawner);
	}

	public List<String> getAllSpawners() {

		List<String> spawners = new ArrayList<String>();
		for (String name : this.spawners.keySet())
			spawners.add(name);
		return spawners;
	}

}
