package me.backstabber.epicsetspawners.stores.spawner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.EntityType;

import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.spawner.VanillaStore;

public class EpicVanillaStore implements VanillaStore {

	private Map<EntityType, SpawnerData> spawners = new HashMap<>();

	@Override
	public void addSpawner(EntityType entity, SpawnerData spawner) {

		spawners.put(entity, spawner);
	}

	@Override
	public boolean isSpawner(EntityType entity) {
		return spawners.containsKey(entity);
	}

	@Override
	public SpawnerData getSpawner(EntityType entity) {

		if (isSpawner(entity))
			return spawners.get(entity);
		return null;
	}

	public List<String> getAllSpawners() {

		List<String> spawners = new ArrayList<String>();
		for (EntityType entity : this.spawners.keySet())
			spawners.add(entity.name().toLowerCase());
		return spawners;
	}

}
