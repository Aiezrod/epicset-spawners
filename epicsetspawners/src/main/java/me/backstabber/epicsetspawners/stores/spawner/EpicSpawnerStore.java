package me.backstabber.epicsetspawners.stores.spawner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.EntityType;

import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.api.stores.spawner.VanillaStore;
import me.backstabber.epicsetspawners.utils.YamlManager;

public class EpicSpawnerStore implements SpawnerStore {

	@Inject
	private EpicSetSpawners plugin;
	@Inject
	private VanillaStore vanilla;
	@Inject
	private ItemStore item;
	@Inject
	private CustomStore custom;
	@Inject
	private VariableStore variable;
	@Inject
	private BlockStore block;

	public void setup() {

		// Setup vanilla spawners
		for (EntityType entity : EntityType.values()) {

			if (entity.isSpawnable() && entity.isAlive()) {

				SpawnerData spawner;
				try {
					spawner=SpawnerBuilder.load(SpawnerType.VANILLA, entity.name().toLowerCase()).getSpawner();
				} catch(IllegalArgumentException e) {
					spawner=SpawnerBuilder.createDefault(SpawnerType.VANILLA, entity.name().toLowerCase()).setEntity(entity).saveToFile().getSpawner();
				}
				vanilla.addSpawner(entity, spawner);
			}
		}
		// setup Item spawners
		for (String fileName : getAllFiles("Spawners/Item")) {
			YamlManager file = new YamlManager(plugin, fileName, "Spawners/Item");
			try {
			SpawnerData spawner = SpawnerBuilder.load(SpawnerType.ITEM, fileName).getSpawner();
			item.addSpawner(fileName, spawner);
			} catch(IllegalArgumentException e) {
				file.getLoc().delete();
			}
		}
		// setup Custom Mob Spawners
		for (String fileName : getAllFiles("Spawners/Custom Mob")) {

			YamlManager file = new YamlManager(plugin, fileName, "Spawners/Custom Mob");
			try {
				SpawnerData spawner = SpawnerBuilder.load(SpawnerType.CUSTOM_MOB, fileName).getSpawner();
				custom.addSpawner(fileName, spawner);
			} catch(IllegalArgumentException e) {
				file.getLoc().delete();
			}
		}
		// setup Block Spawners
		for (String fileName : getAllFiles("Spawners/Block")) {

			YamlManager file = new YamlManager(plugin, fileName, "Spawners/Block");
			try {
				SpawnerData spawner = SpawnerBuilder.load(SpawnerType.BLOCK, fileName).getSpawner();
				block.addSpawner(fileName, spawner);
			} catch(IllegalArgumentException e) {
				file.getLoc().delete();
			}
		}
		// TODO setup variable spawners
	}

	@Override
	public SpawnerData getSpawner(EntityType entity) {

		return vanilla.getSpawner(entity).clone();
	}

	@Override
	public SpawnerData getSpawner(String spawnerName) {
		spawnerName = spawnerName.toLowerCase();
		if (item.isSpawner(spawnerName))
			return item.getSpawner(spawnerName).clone();
		if (custom.isSpawner(spawnerName))
			return custom.getSpawner(spawnerName).clone();
		if (block.isSpawner(spawnerName))
			return block.getSpawner(spawnerName).clone();
		if (variable.isSpawner(spawnerName))
			return variable.getSpawner(spawnerName).clone();
		try {
			EntityType entity = EntityType.valueOf(spawnerName.toUpperCase());
			if (vanilla.isSpawner(entity))
				return vanilla.getSpawner(entity);
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public boolean isSpawner(String spawnerName) {

		if (item.isSpawner(spawnerName))
			return true;
		if (custom.isSpawner(spawnerName))
			return true;
		if (block.isSpawner(spawnerName))
			return true;
		if (variable.isSpawner(spawnerName))
			return true;
		try {
			EntityType entity = EntityType.valueOf(spawnerName.toUpperCase());
			if (vanilla.isSpawner(entity))
				return true;
		} catch (Exception e) {
		}
		return false;
	}
	@Override
	public boolean isCustomSpawner(String spawnerName) {

		if (item.isSpawner(spawnerName))
			return true;
		if (custom.isSpawner(spawnerName))
			return true;
		if (block.isSpawner(spawnerName))
			return true;
		if (variable.isSpawner(spawnerName))
			return true;
		return false;
	}
	
	@Override
	public boolean isSpawner(EntityType entity) {
		return vanilla.isSpawner(entity);
	}
	public void defaultAll() {
		for(String name:getAllSpawners()) {
			applyDefault(name);
		}
	}
	public void applyDefault(String name) {
		if(getSpawner(name)!=null) {
			SpawnerData spawner=getSpawner(name);
			SpawnerBuilder builder=SpawnerBuilder.load(spawner.getType(), name).applyDefault().saveToFile();
			switch (spawner.getType()) {

			case ITEM:
				item.addSpawner(name, builder.getSpawner());
				break;

			case CUSTOM_MOB:
				custom.addSpawner(name, builder.getSpawner());
				break;

			case BLOCK:
				block.addSpawner(name, builder.getSpawner());
				break;

			case VARIABLE:
				variable.addSpawner(name, builder.getSpawner());
				break;

			case VANILLA:
				vanilla.addSpawner(EntityType.valueOf(name.toUpperCase()), builder.getSpawner());
				break;

			default:
				break;
			}
		}
	}
	public void addSpawner(String spawnerName, SpawnerBuilder builder) {
		spawnerName = spawnerName.toLowerCase();
		if (isSpawner(spawnerName))
			return;
		SpawnerData spawner = builder.setName(spawnerName).saveToFile().getSpawner();
		switch (spawner.getType()) {

		case ITEM:
			item.addSpawner(spawnerName, spawner);
			break;

		case CUSTOM_MOB:
			custom.addSpawner(spawnerName, spawner);
			break;

		case BLOCK:
			block.addSpawner(spawnerName, spawner);
			break;

		case VARIABLE:
			variable.addSpawner(spawnerName, spawner);
			break;

		case VANILLA:
			return;

		default:
			break;
		}
	}

	public void addSpawner(String spawnerName, SpawnerData spawner) {
		spawnerName = spawnerName.toLowerCase();
		if (isSpawner(spawnerName))
			return;
		switch (spawner.getType()) {

		case ITEM:
			item.addSpawner(spawnerName, spawner);
			break;

		case CUSTOM_MOB:
			custom.addSpawner(spawnerName, spawner);
			break;

		case BLOCK:
			block.addSpawner(spawnerName, spawner);
			break;

		case VARIABLE:
			variable.addSpawner(spawnerName, spawner);
			break;

		case VANILLA:
			return;

		default:
			break;
		}
	}

	public void removeSpawner(String spawnerName) {
		spawnerName = spawnerName.toLowerCase();
		if (item.isSpawner(spawnerName))
			item.removeSpawner(spawnerName);
		if (custom.isSpawner(spawnerName))
			custom.removeSpawner(spawnerName);
		if (block.isSpawner(spawnerName))
			block.removeSpawner(spawnerName);
		if (variable.isSpawner(spawnerName))
			variable.removeSpawner(spawnerName);

	}

	public List<String> getAllSpawners() {
		List<String> spawners = new ArrayList<String>();
		spawners.addAll(((EpicVanillaStore) vanilla).getAllSpawners());
		spawners.addAll(item.getAllSpawners());
		spawners.addAll(custom.getAllSpawners());
		spawners.addAll(block.getAllSpawners());
		spawners.addAll(variable.getAllSpawners());
		return spawners;
	}

	public List<String> getCustomSpawners() {
		List<String> spawners = new ArrayList<String>();
		spawners.addAll(item.getAllSpawners());
		spawners.addAll(custom.getAllSpawners());
		spawners.addAll(block.getAllSpawners());
		spawners.addAll(variable.getAllSpawners());
		return spawners;
	}

	/*
	 * Purpose of this method is to fetch all files stored in the /plugin folder
	 */
	public List<String> getAllFiles(String path) {
		File folder = new File(plugin.getDataFolder() + "/" + path);
		String[] fileNames = folder.list();
		ArrayList<String> names = new ArrayList<String>();
		if (fileNames != null) {
			for (String s : fileNames) {
				names.add(s.replace(".yml", ""));
			}
		}
		return names;
	}

}
