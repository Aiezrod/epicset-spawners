package me.backstabber.epicsetspawners.stores.location;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.YamlManager;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;

public class EpicLocationStore implements LocationStore {

	@Inject
	private EpicSetSpawners plugin;
	private YamlManager file;
	private Map<Block, SpawnerData> spawners = new HashMap<Block, SpawnerData>();

	public void setup() {
		file = new YamlManager(plugin, "LocationStore");
		for (String locationString : file.getFile().getKeys(false)) {
			Block block = CommonUtils.getBlockLocation(locationString).getBlock();
			if (block.getState() instanceof CreatureSpawner) {
				String tag = file.getFile().getString(locationString);
				JsonParser parser=new JsonParser();
				JsonObject json=(JsonObject) parser.parse(tag);
				if(json.get("EpicSetSpawner")!=null) {
					SpawnerType type=SpawnerType.valueOf(json.get("Type").getAsString().toUpperCase());
					String name=json.get("Name").getAsString();
					SpawnerData spawner;
					try {
						spawner=SpawnerBuilder.load(type, name).getSpawner(json);
					} catch (IllegalArgumentException e) {
						block.setType(EpicMaterials.valueOf("AIR").getMaterial());
						Location standLocation = block.getLocation().add(0.5, -1, 0.5);
						for (Entity entity : standLocation.getWorld().getNearbyEntities(standLocation, 0.5, 0.5, 0.5))
							if (entity instanceof ArmorStand)
								entity.remove();
						file.getFile().set(locationString, null);
						continue;
					}
					spawner.applyToBlock(block, null);
					
				}
			} else {
				file.getFile().set(locationString, null);
			}
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				for(Block block:spawners.keySet()) {
					if(spawners.get(block).getType().equals(SpawnerType.VARIABLE)) {
						((EpicSpawnerData)spawners.get(block)).addSecond(block);
					}
				}
			}
		}.runTaskTimer(plugin, 20, 20);
	}

	@Override
	public void addSpawner(SpawnerData spawner, Block block) {

		spawners.put(block, spawner);
		file.getFile().set(CommonUtils.getBlockString(block.getLocation()), ((EpicSpawnerData)spawner).getJson().toString());
		file.save(false);
	}

	@Override
	public boolean isStored(Block block) {

		return spawners.containsKey(block);
	}

	@Override
	public SpawnerData fetch(Block block) {

		return spawners.get(block);
	}

	public Set<Block> getAllBlocks() {
		return this.spawners.keySet();
	}
	public void removeBlock(Block block) {

		spawners.remove(block);
		file.getFile().set(CommonUtils.getBlockString(block.getLocation()), null);
		file.save(false);
	}

}
