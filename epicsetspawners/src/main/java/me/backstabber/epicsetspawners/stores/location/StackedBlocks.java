package me.backstabber.epicsetspawners.stores.location;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.YamlManager;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;

public class StackedBlocks {

	@Inject
	private EpicSetSpawners plugin;
	private YamlManager file;
	private Map<Block, Integer> blocks = new HashMap<Block, Integer>();

	public void setup() {

		file = new YamlManager(plugin, "BlockStore");
		for (String locationString : file.getFile().getKeys(false)) {
			Block block = CommonUtils.getBlockLocation(locationString).getBlock();
			if (!block.getType().equals(EpicMaterials.valueOf("AIR").getMaterial())&&block.getType().isBlock()) {
				int amount=file.getFile().getInt(locationString);
				setBlock(block, amount);
			} else {
				file.getFile().set(locationString, null);
			}
		}
	}

	public void setBlock(Block block,int amount) {

		blocks.put(block, amount);
		addTag(block, amount);
		file.getFile().set(CommonUtils.getBlockString(block.getLocation()), amount);
		file.save(false);
	}
	public boolean isStored(Block block) {

		return blocks.containsKey(block);
	}

	public int fetch(Block block) {

		return blocks.get(block);
	}

	public void removeBlock(Block block) {

		blocks.remove(block);
		Location standlLocation=block.getLocation().add(0.5,1,0.5);
		for(Entity entity:block.getWorld().getNearbyEntities(standlLocation, 0.1, 0.1, 0.1))
			if(entity instanceof ArmorStand)
				entity.remove();
		file.getFile().set(CommonUtils.getBlockString(block.getLocation()), null);
		file.save(false);
	}
	@SuppressWarnings("deprecation")
	private void addTag(Block block,int amount) {
		Location standlLocation=block.getLocation().add(0.5,1,0.5);
		for(Entity entity:block.getWorld().getNearbyEntities(standlLocation, 0.1, 0.1, 0.1))
			if(entity instanceof ArmorStand)
				entity.remove();
		ArmorStand stand=(ArmorStand) block.getWorld().spawnEntity(standlLocation, EntityType.ARMOR_STAND);
		stand.setGravity(false);
		stand.setVisible(false);
		stand.setSmall(true);
		String tag=plugin.getSettings().getFile().getString("settings.block-stacker.format");
		tag=tag.replace("%block%", CommonUtils.format(block.getType().name()));
		tag=tag.replace("%amount%", CommonUtils.getDecimalFormatted(amount));
		stand.setCustomName(CommonUtils.chat(tag));
		stand.setCustomNameVisible(true);
		stand.setHelmet(EpicMaterials.valueOf(block).getItemStack());
	}
}
