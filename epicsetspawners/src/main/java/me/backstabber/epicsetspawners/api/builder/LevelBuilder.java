package me.backstabber.epicsetspawners.api.builder;

import org.bukkit.configuration.file.FileConfiguration;

import me.backstabber.epicsetspawners.api.builder.validate.UpgradePaths;

public class LevelBuilder {

	private int money;
	private int xp;
	private int minDelay;
	private int maxDelay;
	private int spawnCount;
	public LevelBuilder() {
		money=20000;
		xp=2000;
		minDelay=200;
		maxDelay=500;
		spawnCount=5;
	}
	public static LevelBuilder create() {
		return new LevelBuilder();
	}
	public LevelBuilder setMoneyPrice(int money) {
		this.money=money;
		return this;
	}
	public LevelBuilder setXpPrice(int xp) {
		this.xp=xp;
		return this;
	}
	public LevelBuilder setMinDelay(int minDelay) {
		this.minDelay=minDelay;
		return this;
	}
	public LevelBuilder setMaxDelay(int maxDelay) {
		this.maxDelay=maxDelay;
		return this;
	}
	public LevelBuilder setSpawnCount(int spawnCount) {
		this.spawnCount=spawnCount;
		return this;
	}
	public void apply(int level,FileConfiguration file) {
		file.set(UpgradePaths.LEVELS.getPath()+"."+level+".cost.money", money);
		file.set(UpgradePaths.LEVELS.getPath()+"."+level+".cost.exp", xp);
		file.set(UpgradePaths.LEVELS.getPath()+"."+level+".upgraded-settings.min-delay", minDelay);
		file.set(UpgradePaths.LEVELS.getPath()+"."+level+".upgraded-settings.max-delay", maxDelay);
		file.set(UpgradePaths.LEVELS.getPath()+"."+level+".upgraded-settings.spawn-count", spawnCount);
	}
}
