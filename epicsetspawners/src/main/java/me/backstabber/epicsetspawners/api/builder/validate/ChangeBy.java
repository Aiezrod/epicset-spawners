package me.backstabber.epicsetspawners.api.builder.validate;

public enum ChangeBy {
	PLACEHOLDER,
	SPAWN,
	TIME;
}
