package me.backstabber.epicsetspawners.api.builder.validate;

import org.bukkit.configuration.file.FileConfiguration;

import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;

public class VariableTypes {
	private int max;
	private int min;
	private SpawnerData spawner;
	public VariableTypes(int min,int max,SpawnerData data) {
		if(min>max)
			throw new IllegalArgumentException("Minimum cant be greater than maximim.");
		if(data.getType().equals(SpawnerType.VARIABLE))
			throw new IllegalArgumentException("Cannot have variable spawner as data.");
		this.min=min;
		this.max=max;
		this.spawner=data;
	}
	public boolean isOverlap(double value) {
		return (value>min&&value<=max);
	}
	public boolean isOverlap(VariableTypes data) {
		return (isOverlap(data.min)||isOverlap(data.max));
	}
	public SpawnerData getSpawner() {
		return this.spawner;
	}
	public void apply(int level,FileConfiguration file) {
		file.set(VariablePaths.VALUES.getPath()+"."+level+".min",min);
		file.set(VariablePaths.VALUES.getPath()+"."+level+".max",max);
		file.set(VariablePaths.VALUES.getPath()+"."+level+".spawner",((EpicSpawnerData)spawner).getName());
	}
	public Integer getMax() {
		return max;
	}
}
