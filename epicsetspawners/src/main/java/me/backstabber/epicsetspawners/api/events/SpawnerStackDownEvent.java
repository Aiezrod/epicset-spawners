package me.backstabber.epicsetspawners.api.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public class SpawnerStackDownEvent extends PlayerEvent implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private Player placer;
	private Block block;
	private SpawnerData spawner;
	private boolean canceled = false;
	private ItemStack drop;

	public SpawnerStackDownEvent(Player placer, Block block, ItemStack drop, SpawnerData spawner) {
		super(placer);
		this.placer = placer;
		this.spawner = spawner;
		this.block = block;
		this.drop = drop;
	}

	public Player getPlacer() {
		return placer;
	}

	public SpawnerData getSpawner() {
		return spawner;
	}

	public Block getBlock() {
		return block;
	}

	public ItemStack getDrop() {
		return drop;
	}

	public void setDrop(ItemStack drop) {
		this.drop = drop;
	}

	@Override
	public boolean isCancelled() {
		return canceled;
	}

	@Override
	public void setCancelled(boolean canceled) {
		this.canceled = canceled;

	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static final HandlerList getHandlerList() {
		return handlers;
	}

}
