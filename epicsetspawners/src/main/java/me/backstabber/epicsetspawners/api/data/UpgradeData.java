package me.backstabber.epicsetspawners.api.data;

import java.util.Map;

public interface UpgradeData {
	/*
	 * Upgrade Data will contain all data related to an upgrade-able spawner. This
	 * included level , max-level , & settings on each level
	 */

	/*
	 * Check if the spawner is upgrade-able This method will only check if upgrading
	 * is enabled or not It does'nt check for maxed out spawners
	 */
	public boolean isUpgradable();

	/*
	 * Check if Spawner is maxed out
	 */

	public boolean isMaxed();

	/*
	 * Get current level of the spawner
	 */

	public int getLevel();

	/*
	 * Get max level of the spawner
	 */

	public int getMaxLevel();

	/*
	 * This method will return data related to the current level The data is: 1)
	 * Mob-count 2) Min Spawn Delay 3) Max Spawn Delay
	 */
	public Map<DataType, Integer> getLevelData();

	/*
	 * This method will return same data but for a specified level
	 */
	public Map<DataType, Integer> getLevelData(int level);

	/*
	 * Method to change data parameters for a spawner works only for un-upgradable
	 * spawners
	 */

	public void changeParameter(DataType parameter, int data);

	public enum DataType {
		MOB_COUNT, MIN_DELAY, MAX_DELAY;
	}
}
