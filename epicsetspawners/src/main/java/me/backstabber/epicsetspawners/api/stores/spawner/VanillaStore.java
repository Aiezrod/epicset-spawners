package me.backstabber.epicsetspawners.api.stores.spawner;

import org.bukkit.entity.EntityType;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public interface VanillaStore {
	/*
	 * Vanilla store will contain all vanilla spawners It will have methods to fetch
	 * the Spawner object for every living EntityType upon initializing all Spawner
	 * objects will be given to it from the SpawnerBuilder
	 */
	/*
	 * All stores will have 3 methods 1) Method to check if a name = spawner 2)
	 * Method to fetch a Spawner Object from Name 3) Method to remove a Spawner from
	 * the store (not for Vanilla Spawners)
	 */

	/*
	 * These Methods will be package specific so only SpawnerStore can access them
	 */
	void addSpawner(EntityType entity, SpawnerData spawner);

	boolean isSpawner(EntityType entity);

	SpawnerData getSpawner(EntityType entity);
}
