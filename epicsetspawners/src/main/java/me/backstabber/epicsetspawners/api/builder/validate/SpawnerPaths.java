package me.backstabber.epicsetspawners.api.builder.validate;

import java.util.ArrayList;
import java.util.List;

public enum SpawnerPaths {
	// basic spawner settings
	SETTINGS_TYPE("spawn-settings.type", "VANILLA"),
	SETTINGS_ENTITY("spawn-settings.entity", "PIG"),
	SETTINGS_NBT_DATA("spawn-settings.nbt-data", "{}"),
	SETTINGS_MIN_DELAY("spawn-settings.min-delay", 200),
	SETTINGS_MAX_DELAY("spawn-settings.max-delay", 600),
	SETTINGS_SPAWN_COUNT("spawn-settings.spawn-count", 5),
	// spawner breaking settings

	BREAKING_TOOL_ENABLED("spawner-breaking.tool-requirement.enabled", true),
	BREAKING_TOOL_TYPES("spawner-breaking.tool-requirement.tools", "pickaxe", ""),
	BREAKING_TOOL_ENCHANTS("spawner-breaking.tool-requirement.enchants", "silk_touch", ""),
	BREAKING_PERMS_ENABLED("spawner-breaking.permission-requirement.enabled", false),
	BREAKING_PERMS_PERM("spawner-breaking.permission-requirement.permissions", "epicspawners.breaking"),
	BREAKING_IN_INVENTORY("spawner-breaking.place-inside-inventory", false),
	// message settings

	MESSAGE_ONBREAK("messages.onbreak-spawner", "&eBroke &a%stack%x %entity%&e spawner"),
	MESSAGE_ONPLACE("messages.onplace-spawner", "&ePlaced &a%stack%x %entity%&e spawner"),
	// formats settings

	SPAWNER_STACKED_NAME("formats.spawner-nametag", "&e%stack%&7x %entity%"),
	SPAWNER_ITEM_TYPE("formats.spawner-item.type", "SPAWNER"),
	SPAWNER_ITEM_NAME("formats.spawner-item.name", "&e%stack%&7x %entity% Spawner"),
	SPAWNER_ITEM_LORE("formats.spawner-item.lore", "", "");

	private String path;
	private Object defaultData;

	SpawnerPaths(String path, Object defaultData) {
		this.path = path;
		this.defaultData = defaultData;
	}

	SpawnerPaths(String path, String... array) {
		this.path = path;
		List<String> data = new ArrayList<String>();
		for (String s : array)
			data.add(s);
		this.defaultData = data;
	}

	public String getPath() {
		return this.path;
	}

	public Object getDefault() {
		return this.defaultData;
	}
}
