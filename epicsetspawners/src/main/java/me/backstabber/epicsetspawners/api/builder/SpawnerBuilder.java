package me.backstabber.epicsetspawners.api.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import com.google.gson.JsonObject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.validate.SpawnerPaths;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.UpgradeData;
import me.backstabber.epicsetspawners.api.data.UpgradeData.DataType;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.YamlManager;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;

	/*
	 * The purpose of this builder is to build up Spawner object from .yml files.
	 * and recreate .yml files from Spawner objects.
	 */
	
	/*
	 * this method will return Spawner Object from a given Yaml file
	 */
public class SpawnerBuilder {

	//private FileConfiguration file;
	
	private String name;
	private SpawnerType type;
	private EntityType entity;
	private String nbtTags;
	private Map<DataType, Integer> data=new HashMap<UpgradeData.DataType, Integer>();
	private String breakMessage;
	private String placeMessage;
	
	private GuiBuilder gui;
	private UpgradeBuilder upgrade;
	private PermissionsBuilder permissions;
	
	private VariableBuilder variable;

	private String itemType;
	private String itemName;
	private List<String> itemLore=new ArrayList<String>();
	private String spawnerTag;

	public static SpawnerBuilder create(SpawnerType type,String name) {
		SpawnerBuilder builder=new SpawnerBuilder();
		builder.type=type;
		builder.name=name;
		return builder;
	}
	public static SpawnerBuilder createDefault(SpawnerType type,String name) {
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		SpawnerBuilder builder=new SpawnerBuilder();
		builder.type=type;
		builder.name=name;
		builder.setEntity(EntityType.PIG)
		.setNbtTags("{}")
		.setData(DataType.MOB_COUNT, plugin.getSettings().getFile().getInt("default-values.spawn-count"))
		.setData(DataType.MIN_DELAY, plugin.getSettings().getFile().getInt("default-values.min-delay"))
		.setData(DataType.MAX_DELAY, plugin.getSettings().getFile().getInt("default-values.max-delay"))
		.setBreakMessage(plugin.getSettings().getFile().getString("default-values.message-onbreak"))
		.setPlaceMessage(plugin.getSettings().getFile().getString("default-values.message-onplace"))
		.setGui(GuiBuilder.createDefault())
		.setUpgrades(UpgradeBuilder.create())
		.setPerms(PermissionsBuilder.createDefault())
		.setSpawnerItem(plugin.getSettings().getFile().getString("default-values.spawner-item.type")
				, plugin.getSettings().getFile().getString("default-values.spawner-item.name")
				, plugin.getSettings().getFile().getStringList("default-values.spawner-item.lore"))
		.setSpawerTag(plugin.getSettings().getFile().getString("default-values.spawner-nametag"))
		;
		return builder;
	}
	public static SpawnerBuilder load(SpawnerType type,String name) {
		YamlManager manager = type.getFile(name);
		for(SpawnerPaths path:SpawnerPaths.values())
			if(!manager.getFile().isSet(path.getPath()))
				throw new IllegalArgumentException("No such spawner exists");
		SpawnerBuilder builder=new SpawnerBuilder();
		builder.name=name;
		FileConfiguration file = manager.getFile();
		//add spawnerPaths
		builder.type=SpawnerType.valueOf(file.getString(SpawnerPaths.SETTINGS_TYPE.getPath()).toUpperCase());
		builder.setEntity(EntityType.valueOf(file.getString(SpawnerPaths.SETTINGS_ENTITY.getPath()).toUpperCase()));
		builder.setData(DataType.MIN_DELAY, file.getInt(SpawnerPaths.SETTINGS_MIN_DELAY.getPath()));
		builder.setData(DataType.MAX_DELAY, file.getInt(SpawnerPaths.SETTINGS_MAX_DELAY.getPath()));
		builder.setData(DataType.MOB_COUNT, file.getInt(SpawnerPaths.SETTINGS_SPAWN_COUNT.getPath()));
		builder.setNbtTags(file.getString(SpawnerPaths.SETTINGS_NBT_DATA.getPath()));

		builder.itemType=file.getString(SpawnerPaths.SPAWNER_ITEM_TYPE.getPath());
		builder.itemName=file.getString(SpawnerPaths.SPAWNER_ITEM_NAME.getPath());
		builder.itemLore=file.getStringList(SpawnerPaths.SPAWNER_ITEM_LORE.getPath());
		builder.spawnerTag=file.getString(SpawnerPaths.SPAWNER_STACKED_NAME.getPath());
		builder.breakMessage=file.getString(SpawnerPaths.MESSAGE_ONBREAK.getPath());
		builder.placeMessage=file.getString(SpawnerPaths.MESSAGE_ONPLACE.getPath());
		
		builder.gui=GuiBuilder.load(type, name);
		builder.permissions=PermissionsBuilder.load(type, name);
		builder.upgrade=UpgradeBuilder.load(type, name);
		return builder;
	}
	public SpawnerBuilder applyDefault() {
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		this
		.setData(DataType.MOB_COUNT, plugin.getSettings().getFile().getInt("default-values.spawn-count"))
		.setData(DataType.MIN_DELAY, plugin.getSettings().getFile().getInt("default-values.min-delay"))
		.setData(DataType.MAX_DELAY, plugin.getSettings().getFile().getInt("default-values.max-delay"))
		.setBreakMessage(plugin.getSettings().getFile().getString("default-values.message-onbreak"))
		.setPlaceMessage(plugin.getSettings().getFile().getString("default-values.message-onplace"))
		.setSpawnerItem(plugin.getSettings().getFile().getString("default-values.spawner-item.type")
				, plugin.getSettings().getFile().getString("default-values.spawner-item.name")
				, plugin.getSettings().getFile().getStringList("default-values.spawner-item.lore"))
		.setSpawerTag(plugin.getSettings().getFile().getString("default-values.spawner-nametag"))
		;
		this.gui.applyDefault();
		this.upgrade.applyDefault();
		return this;
	}
	
	public SpawnerBuilder setType(SpawnerType type) {
		this.type=type;
		return this;
	}
	public SpawnerBuilder setName(String name) {
		if(!this.type.equals(SpawnerType.VANILLA))
			this.name=name.toLowerCase();
		return this;
	}
	/**
	 * 
	 * @param type of entity (must be living)
	 * @return builder with entity set
	 */
	public SpawnerBuilder setEntity(EntityType entity) {
		this.entity=entity;
		if(this.type.equals(SpawnerType.VANILLA))
			this.name=entity.name().toLowerCase();
		return this;
	}

	/**
	 * 
	 * @param String containing all nbt data for spawned entity
	 * @return Builder with new settings
	 */
	public  SpawnerBuilder setNbtTags(String nbtTags) {
		this.nbtTags=nbtTags;
		return this;
	}
	
	/**
	 *  Set data parameters for the spawner
	 * @param Type of data to set
	 * @param Value for the data
	 * @return Builder with new data
	 */
	public SpawnerBuilder setData(DataType type,int data) {
		this.data.put(type, data);
		return this;
	}
	public SpawnerBuilder setSpawnerItem(ItemStack spawner) {
		if(spawner.hasItemMeta()&&spawner.getItemMeta().hasItemFlag(ItemFlag.HIDE_ENCHANTS))
			itemType="<glow>"+EpicMaterials.valueOf(spawner).name();
		else
			itemType= EpicMaterials.valueOf(spawner).name();
		itemName= CommonUtils.getItemName(spawner);
		if(spawner.hasItemMeta()&&spawner.getItemMeta().hasLore())
			itemLore=spawner.getItemMeta().getLore();
		return this;
	}
	public SpawnerBuilder setSpawnerItem(String type,String name,List<String> lore) {
		itemType=type;
		itemName=name;
		itemLore=lore;
		return this;
	}
	public SpawnerBuilder setSpawerTag(String tag) {
		this.spawnerTag=tag;
		return this;
	}
	/*
	 * Set Messages for the spawner
	 */
	public SpawnerBuilder setBreakMessage(String message) {
		this.breakMessage=message;
		return this;
	}
	public SpawnerBuilder setPlaceMessage(String message) {
		this.placeMessage=message;
		return this;
	}
	
	/**
	 * Adds a Gui to this spawner
	 * @param Gui related data (Created using GuiBuilder)
	 * @return Builder with new gui
	 */
	public SpawnerBuilder setGui(GuiBuilder gui) {
		this.gui=gui;
		return this;
	}
	
	public SpawnerBuilder setVariable(VariableBuilder builder) {
		this.variable=builder;
		return this;
	}
	
	public VariableBuilder getVariable() {
		return this.variable;
	}
	
	/**
	 * Adds upgrades to this Spawner
	 * @param Upgrade related data (created using UpgradeBuilder)
	 * @return Builder with upgrades
	 */
	public SpawnerBuilder setUpgrades(UpgradeBuilder upgrade) {
		this.upgrade=upgrade;
		return this;
	}
	
	/**
	 * Adds breaking permissions to the spawner
	 * @param PermissionBuilder
	 * @return Builder with perms
	 */
	public SpawnerBuilder setPerms(PermissionsBuilder permissions) {
		this.permissions=permissions;
		return this;
	}
	
	public SpawnerData getSpawner() {
		YamlManager manager = type.getFile(name);
		SpawnerData data = new EpicSpawnerData(manager.getFile(), name);
		EpicSetSpawners.getPlugin(EpicSetSpawners.class).injectMembers(data);
		return data;
	}
	
	public SpawnerData getSpawner(JsonObject json) {
		YamlManager manager = type.getFile(name);
		SpawnerData data=new EpicSpawnerData(manager.getFile(), name);
		EpicSetSpawners.getPlugin(EpicSetSpawners.class).injectMembers(data);
		((EpicSpawnerData)data).addData(json);
		return data;
	}
	public SpawnerBuilder saveToFile() {
		if(this.type.equals(SpawnerType.VANILLA))
			this.name=entity.name().toLowerCase();
		YamlManager manager = type.getFile(name);
		FileConfiguration file=manager.getFile();
		//add spawnerPaths
		file.set(SpawnerPaths.SETTINGS_TYPE.getPath(), type.name());
		file.set(SpawnerPaths.SETTINGS_ENTITY.getPath(), entity.name());
		file.set(SpawnerPaths.SETTINGS_MIN_DELAY.getPath(), data.get(DataType.MIN_DELAY));
		file.set(SpawnerPaths.SETTINGS_MAX_DELAY.getPath(), data.get(DataType.MAX_DELAY));
		file.set(SpawnerPaths.SETTINGS_SPAWN_COUNT.getPath(), data.get(DataType.MOB_COUNT));
		file.set(SpawnerPaths.SETTINGS_NBT_DATA.getPath(), nbtTags);

		file.set(SpawnerPaths.SPAWNER_ITEM_TYPE.getPath(), itemType);
		file.set(SpawnerPaths.SPAWNER_ITEM_NAME.getPath(), itemName);
		file.set(SpawnerPaths.SPAWNER_ITEM_LORE.getPath(), itemLore);
		file.set(SpawnerPaths.SPAWNER_STACKED_NAME.getPath(), spawnerTag);
		file.set(SpawnerPaths.MESSAGE_ONBREAK.getPath(), breakMessage);
		file.set(SpawnerPaths.MESSAGE_ONPLACE.getPath(), placeMessage);
		
		gui.applyToFile(file);
		permissions.applyToFile(file);
		upgrade.applyToFile(file);
		if(this.type.equals(SpawnerType.VARIABLE)&&this.variable!=null)
			variable.applyToFile(file);
		manager.save(true);
		return this;
	}
	public enum SpawnerType {
		/*
		 * This Enum will aid in the identification of spawner types so its easier to
		 * assign the spawner data to different stores
		 */
		VANILLA("Spawners/Vanilla"),
		ITEM("Spawners/Item"), 
		CUSTOM_MOB("Spawners/Custom Mob"), 
		BLOCK("Spawners/Block"), 
		VARIABLE("Spawners/Variable");
		private String location;
		SpawnerType(String location) {
			this.location=location;
		}
		public YamlManager getFile(String name) {
			return new YamlManager(EpicSetSpawners.getPlugin(EpicSetSpawners.class), name,location);
		}
	}
}
