package me.backstabber.epicsetspawners.api.events;

import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public class SpawnerExplodeEvent extends BlockEvent implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private SpawnerData spawner;
	private boolean canceled = false;
	private ItemStack drop;

	public SpawnerExplodeEvent(Block block, ItemStack drop, SpawnerData spawner) {
		super(block);
		this.spawner = spawner;
		this.drop = drop;
	}

	public SpawnerData getSpawner() {
		return spawner;
	}

	public ItemStack getDrop() {
		return drop;
	}

	public void setDrop(ItemStack drop) {
		this.drop = drop;
	}

	@Override
	public boolean isCancelled() {
		return canceled;
	}

	@Override
	public void setCancelled(boolean canceled) {
		this.canceled = canceled;

	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static final HandlerList getHandlerList() {
		return handlers;
	}

}
