package me.backstabber.epicsetspawners.api.stores.spawner;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public interface NonVanillaStore {
	/*
	 * NonVanillaStore will contain all spawners other than Vanilla. It will have
	 * multiple implementations depending on the number of categories the plugin has
	 * It will have methods to fetch the Spawner object for a predefined spawner
	 * name. upon initializing all Spawner objects will be given to it from the
	 * SpawnerBuilder
	 */
	/*
	 * All stores will have 3 methods 1) Method to check if a name = spawner 2)
	 * Method to fetch a Spawner Object from Name 3) Method to remove a Spawner from
	 * the store (not for Vanilla Spawners)
	 */

	/*
	 * These Methods will be package specific so only SpawnerStore can access them
	 */
	void addSpawner(String spawnerName, SpawnerData spawner);

	boolean isSpawner(String spawnerName);

	void removeSpawner(String spawnerName);

	SpawnerData getSpawner(String SpawnerName);
}
