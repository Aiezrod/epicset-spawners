package me.backstabber.epicsetspawners.api.builder.validate;

import java.util.ArrayList;
import java.util.List;

public enum GuiPaths {

	ENABLED("gui.enabled", true), 
	GUI_NAME("gui.name", "Spawner Info"), 
	GUI_SIZE("gui.size", 3),
	BACKGROUND_TYPE("gui.background.type", "<glow>BLACK_STAINED_GLASS_PANE"),
	BACKGROUND_NAME("gui.background.name", "&aSpawner Info"),
	BACKGROUND_LORE("gui.background.lore", "&7View information about", "&7this spawner."),
	ITEMS("gui.items");

	private String path;
	private Object defaultData;

	GuiPaths(String path) {
		this.path = path;
		this.defaultData = null;
	}

	GuiPaths(String path, Object defaultData) {
		this.path = path;
		this.defaultData = defaultData;
	}

	GuiPaths(String path, String... array) {
		this.path = path;
		List<String> data = new ArrayList<String>();
		for (String s : array)
			data.add(s);
		this.defaultData = data;
	}

	public String getPath() {
		return this.path;
	}

	public Object getDefault() {
		return this.defaultData;
	}
}
