package me.backstabber.epicsetspawners.api.data;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface GuiData {
	/*
	 * Gui Data is a holder for a spawner's gui if any exists
	 */

	public boolean isEnabled();
	/*
	 * Check if the gui exists
	 */
	public boolean hasGui();

	/*
	 * Open the gui for the player If gui was already open for someone then close it
	 * for the previous player
	 */
	public void openGui(Player player, Block block);

	/*
	 * Check if gui is open for someone
	 */
	public boolean isGuiOpen();

	/*
	 * get the current viewer of this gui
	 */
	public Player getViewer();

}
