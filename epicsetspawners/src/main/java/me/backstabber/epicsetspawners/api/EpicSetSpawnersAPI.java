package me.backstabber.epicsetspawners.api;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.hooks.StackersHook;

public interface EpicSetSpawnersAPI {

	public StackersHook getStacker();

	public SpawnerStore getSpawnerStore();

	public LocationStore getLocationStore();


	public EpicSetSpawners getPlugin();

}
