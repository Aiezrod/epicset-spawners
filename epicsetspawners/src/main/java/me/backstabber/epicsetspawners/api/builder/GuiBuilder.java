package me.backstabber.epicsetspawners.api.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.builder.validate.GuiItem;
import me.backstabber.epicsetspawners.api.builder.validate.GuiPaths;
import me.backstabber.epicsetspawners.data.gui.GuiTriggers;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.YamlManager;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;

public class GuiBuilder {
	boolean enabled=false;
	String guiName="";
	int guiSize=3;
	String backType="";
	String backName="";
	List<String> backLore=new ArrayList<String>();
	Map<Integer, GuiItem> items=new HashMap<Integer, GuiItem>();
	public static GuiBuilder create() {
        return new GuiBuilder();
	}
	public static GuiBuilder load(SpawnerType type,String name) {
		YamlManager manager = type.getFile(name);
		if(!manager.getFile().isSet(GuiPaths.ENABLED.getPath()))
			throw new IllegalArgumentException("Gui Data doesnt exist.");
		GuiBuilder builder=new GuiBuilder();
		builder.enabled=manager.getFile().getBoolean(GuiPaths.ENABLED.getPath());
		builder.guiName=manager.getFile().getString(GuiPaths.GUI_NAME.getPath());
		builder.guiSize=manager.getFile().getInt(GuiPaths.GUI_SIZE.getPath());
		builder.backType=manager.getFile().getString(GuiPaths.BACKGROUND_TYPE.getPath());
		builder.backName=manager.getFile().getString(GuiPaths.BACKGROUND_NAME.getPath());
		builder.backLore=manager.getFile().getStringList(GuiPaths.BACKGROUND_LORE.getPath());
		ConfigurationSection section = manager.getFile().getConfigurationSection(GuiPaths.ITEMS.getPath());
		if(section!=null) {
			for(int i=0;i<builder.guiSize*9;i++) {
				if(section.isSet(i+".trigger")) {
					GuiTriggers trigger=GuiTriggers.valueOf(section.getString(i+".trigger").toUpperCase());
					String itemType=section.getString(i+".type");
					String itemName=section.getString(i+".name");
					List<String> itemLore=section.getStringList(i+".lore");
					builder.items.put(i, new GuiItem(trigger, itemType,itemName,itemLore));
				}
			}
		}
		return builder;
	}
	public static GuiBuilder createDefault() {
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		ConfigurationSection file = plugin.getSettings().getFile().getConfigurationSection("default-values");
		if(!file.isSet(GuiPaths.ENABLED.getPath()))
			throw new IllegalArgumentException("Gui Data doesnt exist.");
		GuiBuilder builder=new GuiBuilder();
		builder.enabled=file.getBoolean(GuiPaths.ENABLED.getPath());
		builder.guiName=file.getString(GuiPaths.GUI_NAME.getPath());
		builder.guiSize=file.getInt(GuiPaths.GUI_SIZE.getPath());
		builder.backType=file.getString(GuiPaths.BACKGROUND_TYPE.getPath());
		builder.backName=file.getString(GuiPaths.BACKGROUND_NAME.getPath());
		builder.backLore=file.getStringList(GuiPaths.BACKGROUND_LORE.getPath());
		ConfigurationSection section = file.getConfigurationSection(GuiPaths.ITEMS.getPath());
		if(section!=null) {
			for(int i=0;i<builder.guiSize*9;i++) {
				if(section.isSet(i+".trigger")) {
					GuiTriggers trigger=GuiTriggers.valueOf(section.getString(i+".trigger").toUpperCase());
					String itemType=section.getString(i+".type");
					String itemName=section.getString(i+".name");
					List<String> itemLore=section.getStringList(i+".lore");
					builder.items.put(i, new GuiItem(trigger, itemType,itemName,itemLore));
				}
			}
		}
		return builder;
	}
	public GuiBuilder applyDefault() {
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		ConfigurationSection file = plugin.getSettings().getFile().getConfigurationSection("default-values");
		if(file==null||!file.isSet(GuiPaths.ENABLED.getPath()))
			return this;
		enabled=file.getBoolean(GuiPaths.ENABLED.getPath());
		guiName=file.getString(GuiPaths.GUI_NAME.getPath());
		guiSize=file.getInt(GuiPaths.GUI_SIZE.getPath());
		backType=file.getString(GuiPaths.BACKGROUND_TYPE.getPath());
		backName=file.getString(GuiPaths.BACKGROUND_NAME.getPath());
		backLore=file.getStringList(GuiPaths.BACKGROUND_LORE.getPath());
		ConfigurationSection section = file.getConfigurationSection(GuiPaths.ITEMS.getPath());
		if(section!=null) {
			items.clear();
			for(int i=0;i<guiSize*9;i++) {
				if(section.isSet(i+".trigger")) {
					GuiTriggers trigger=GuiTriggers.valueOf(section.getString(i+".trigger").toUpperCase());
					String itemType=section.getString(i+".type");
					String itemName=section.getString(i+".name");
					List<String> itemLore=section.getStringList(i+".lore");
					items.put(i, new GuiItem(trigger, itemType,itemName,itemLore));
				}
			}
		}
		return this;
	}
	/**
	 * Enable / Disable the gui
	 * @param enabled
	 * @return
	 */
	public GuiBuilder setEnabled(boolean enabled) {
		this.enabled=enabled;
		return this;
	}
	/**
	 * Set the name of this Gui 
	 * @param name of the gui
	 * @return
	 */
	public GuiBuilder setName(String name) {
		this.guiName=name;
		return this;
	}
	/**
	 * Set the size of the gui
	 * @param size (1-6)
	 * @return
	 */
	public GuiBuilder setSize(int size) {
		this.guiSize=size;
		return this;
	}
	/*
	 * Add background item
	 */
	public GuiBuilder setBackground(String type,String name,List<String> lore) {
		backType=type;
		backName=name;
		backLore=lore;
		return this;
	}
	public GuiBuilder setBackground(ItemStack background) {
		if(background.hasItemMeta()&&background.getItemMeta().hasItemFlag(ItemFlag.HIDE_ENCHANTS))
			backType="<glow>"+EpicMaterials.valueOf(background).name();
		else
			backType= EpicMaterials.valueOf(background).name();
		backName= CommonUtils.getItemName(background);
		if(background.hasItemMeta()&&background.getItemMeta().hasLore())
			backLore=background.getItemMeta().getLore();
		return this;
	}
	/**
	 * Add items into the Gui
	 * @param slot number (0 onwards)
	 * @param item (create new instance of GuiItem )
	 * @return
	 */
	public GuiBuilder addItem(int slot,GuiItem item) {
		this.items.put(slot, item);
		return this;
	}
	/**
	 * Remove items from the Gui
	 * @param slot number (0 onwards)
	 * @return
	 */
	public GuiBuilder removeItem(int slot) {
		this.items.remove(slot);
		return this;
	}
	
	void applyToFile(FileConfiguration file) {
		file.set(GuiPaths.ENABLED.getPath(), enabled);
		file.set(GuiPaths.GUI_NAME.getPath(), guiName);
		file.set(GuiPaths.GUI_SIZE.getPath(), guiSize);
		file.set(GuiPaths.BACKGROUND_TYPE.getPath(), backType);
		file.set(GuiPaths.BACKGROUND_NAME.getPath(), backName);
		file.set(GuiPaths.BACKGROUND_LORE.getPath(), backLore);
		file.set(GuiPaths.ITEMS.getPath(), null);
		for(int slot:items.keySet())
			items.get(slot).apply(slot, file);
	}
	
}
