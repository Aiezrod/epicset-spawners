package me.backstabber.epicsetspawners.api.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public class SpawnerStackUpEvent extends PlayerEvent implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private Player placer;
	private Block block;
	private SpawnerData spawner;
	private boolean canceled = false;

	public SpawnerStackUpEvent(Player placer, Block block, SpawnerData spawner) {
		super(placer);
		this.placer = placer;
		this.spawner = spawner;
		this.block = block;
	}

	public Player getPlacer() {
		return placer;
	}

	public SpawnerData getSpawner() {
		return spawner;
	}

	public Block getBlock() {
		return block;
	}

	@Override
	public boolean isCancelled() {
		return canceled;
	}

	@Override
	public void setCancelled(boolean canceled) {
		this.canceled = canceled;

	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static final HandlerList getHandlerList() {
		return handlers;
	}

}
