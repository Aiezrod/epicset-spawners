package me.backstabber.epicsetspawners.api.builder;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import me.backstabber.epicsetspawners.api.builder.validate.ChangeBy;
import me.backstabber.epicsetspawners.api.builder.validate.VariableTypes;
import me.backstabber.epicsetspawners.api.builder.validate.VariablePaths;
import me.clip.placeholderapi.PlaceholderAPI;

public class VariableBuilder {
	private ChangeBy by;
	private int time;
	private String placeholder;
	private List<VariableTypes> datas=new ArrayList<>(); 
	public static VariableBuilder create() {
		VariableBuilder var = new VariableBuilder();
		return var;
	}
	public VariableBuilder setChangeBy(ChangeBy by) {
		this.by=by;
		return this;
	}
	public VariableBuilder setTime(int seconds) {
		this.time=seconds;
		return this;
	}
	public VariableBuilder setPlaceholder(String placeholder) {
		String value=PlaceholderAPI.setPlaceholders(Bukkit.getOnlinePlayers().iterator().next(), placeholder);
		try {
			Double.valueOf(value);
		}catch(NumberFormatException e) {
			throw new IllegalArgumentException("Placeholder doesnt return a numerical value.");
		}
		this.placeholder=placeholder;
		return this;
	}
	public VariableBuilder addData(VariableTypes data) {
		if(by.equals(ChangeBy.PLACEHOLDER))
			for(VariableTypes var:datas)
				if(var.isOverlap(data))
					throw new IllegalArgumentException("Data cannot overlap with previous entries.");
		this.datas.add(data);
		return this;
	}
	void applyToFile(FileConfiguration file) {
		file.set(VariablePaths.CHANGE_BY.getPath(), by.name());
		file.set(VariablePaths.PLACEHOLDER.getPath(), placeholder);
		file.set(VariablePaths.TIME.getPath(), time);
		for(int i=0;i<datas.size();i++) {
			datas.get(i).apply(i, file);
		}
	}
}
