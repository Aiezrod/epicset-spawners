package me.backstabber.epicsetspawners.api.data;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;

public interface SpawnerData {
	/*
	 * the SpawnerData is the heart of a spawner it will contain all data relevant
	 * to a spawner including its type amount & more
	 */

	/*
	 * Get the ItemStack from the Spawner
	 */
	public ItemStack getItemStack();

	/*
	 * Get the gui data
	 */

	public GuiData getGuiData();

	/*
	 * Get the upgrade Data
	 */

	public UpgradeData getUpgradeData();

	/*
	 * apply this spawner with the current settings on a block.
	 */

	public void applyToBlock(Block block, Player player);

	/*
	 * Change some data of this spawner
	 */

	public void changeParameter(SpawnerParameters parameter, Object data);

	/*
	 * fetch parameters of the spawner
	 */

	public Object getParameter(SpawnerParameters parameter);

	/*
	 * get the spawner type
	 */

	public SpawnerType getType();

	/*
	 * check if the spawner is enabled
	 */

	public boolean isEnabled();

	/*
	 * method to enable/disable the spawner
	 */

	public void setEnabled(boolean enabled);

	/*
	 * method to create a clone of this spawner any modified attributes will not be
	 * cloned
	 */

	//public SpawnerData clone();

	/*
	 * check if 2 spawners are the same
	 */

	public boolean isSimilar(SpawnerData data);

	public enum SpawnerParameters {
		ENTITY,
		/*
		 * Here the amount is the amount of stacked spawners not the spawned mob count.
		 */
		AMOUNT, 
		ENTITY_NBT;
	}
	void setStack(int stack);

	public SpawnerData clone();
}
