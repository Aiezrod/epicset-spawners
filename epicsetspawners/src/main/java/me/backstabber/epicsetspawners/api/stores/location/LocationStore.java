package me.backstabber.epicsetspawners.api.stores.location;

import org.bukkit.block.Block;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public interface LocationStore {
	/*
	 * This store will keep record of all spawners placed on different locations in
	 * game If any spawner is broken or placed its Spawner data will be fetched or
	 * added to this store On Restart all spawners will be loaded from a .yml file
	 * where all Data is saved as a JSON String. On disable all locations will be
	 * saved in the from of JSON strings with reference to their parent Spawner
	 */

	/*
	 * add a spawner to the store
	 */

	public void addSpawner(SpawnerData spawner, Block block);

	/*
	 * Check if a block is a stored spawner
	 */

	public boolean isStored(Block block);

	/*
	 * Fetch the SpawnerData from a block
	 */

	public SpawnerData fetch(Block block);

}
