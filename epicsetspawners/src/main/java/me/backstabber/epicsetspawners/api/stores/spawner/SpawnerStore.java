package me.backstabber.epicsetspawners.api.stores.spawner;

import org.bukkit.entity.EntityType;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public interface SpawnerStore {
	/*
	 * The SpawnerStore will contain all types of Spawner Objects wit reference to
	 * their type i.e Vanilla Spawners will be stored in Vanilla store Item Spawners
	 * in Item store etc an request to fetch a Spawner will be made from this Store
	 * & it will refer the different stores for the requested Spawner
	 */

	/*
	 * Methods to fetch a spawner Vanilla spawners will be saved using EntityType
	 * 
	 * But other spawners will have a lower cased name No two spawners regardless of
	 * type can have the same name. Meaning that an item spawner can't be named
	 * after an entity.
	 */
	public SpawnerData getSpawner(EntityType entity);

	public SpawnerData getSpawner(String spawnerName);

	/*
	 * Method to check if a spawner exists
	 */
	public boolean isSpawner(String spawnerName);

	public boolean isSpawner(EntityType entity);

	public boolean isCustomSpawner(String name);

}
