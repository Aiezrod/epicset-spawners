package me.backstabber.epicsetspawners.api.builder;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.builder.validate.SpawnerPaths;
import me.backstabber.epicsetspawners.utils.UEnchants;
import me.backstabber.epicsetspawners.utils.YamlManager;

public class PermissionsBuilder {
	boolean toolEnabled=false;
	boolean permEnabled=false;
	boolean inInventory=false;
	List<String> toolTypes=new ArrayList<String>();
	List<String> enchantTypes=new ArrayList<String>();
	String permission="*.*";
	public static PermissionsBuilder create() {
		return new PermissionsBuilder();
	}
	public static PermissionsBuilder createDefault() {
		PermissionsBuilder builder=new PermissionsBuilder();
		builder.setToolEnabled(true)
		.addToolType(Material.DIAMOND_PICKAXE)
		.addEnchant(UEnchants.SILK_TOUCH)
		.setPermsEnabled(false)
		.setBreakingPerm("epicsetspawners.breaking")
		;
		return builder;
	}
	public static PermissionsBuilder load(SpawnerType type,String name) {
		YamlManager manager = type.getFile(name);
		if(!manager.getFile().isSet(SpawnerPaths.BREAKING_TOOL_ENABLED.getPath()))
			throw new IllegalArgumentException("Permissions Data doesnt exist.");
		FileConfiguration file = manager.getFile();
		PermissionsBuilder builder=new PermissionsBuilder();
		builder.toolEnabled=file.getBoolean(SpawnerPaths.BREAKING_TOOL_ENABLED.getPath());
		builder.toolTypes=file.getStringList(SpawnerPaths.BREAKING_TOOL_TYPES.getPath());
		builder.enchantTypes=file.getStringList(SpawnerPaths.BREAKING_TOOL_ENCHANTS.getPath());
		builder.permEnabled=file.getBoolean(SpawnerPaths.BREAKING_PERMS_ENABLED.getPath());
		builder.permission=file.getString(SpawnerPaths.BREAKING_PERMS_PERM.getPath());
		builder.inInventory=file.getBoolean(SpawnerPaths.BREAKING_IN_INVENTORY.getPath());
		
		return builder;
	}
	public PermissionsBuilder setToolEnabled(boolean enabled) {
		this.toolEnabled=enabled;
		return this;
	}
	public PermissionsBuilder addToolType(Material tool) {
		toolTypes.add(getToolName(tool));
		return this;
	}
	public PermissionsBuilder removeToolType(Material tool) {
		toolTypes.remove(getToolName(tool));
		return this;
	}
	public PermissionsBuilder addEnchant(UEnchants enchant) {
		enchantTypes.add(enchant.name());
		return this;
	}
	public PermissionsBuilder removeEnchant(UEnchants enchant) {
		enchantTypes.remove(enchant.name());
		return this;
	}
	
	public PermissionsBuilder setPermsEnabled(boolean enabled) {
		this.permEnabled=enabled;
		return this;
	}
	public PermissionsBuilder setBreakingPerm(String permission ) {
		this.permission=permission;
		return this;
	}
	public PermissionsBuilder setPlaceInInventory(boolean enabled) {
		this.inInventory=enabled;
		return this;
	}
	void applyToFile(FileConfiguration file) {
		file.set(SpawnerPaths.BREAKING_TOOL_ENABLED.getPath(), toolEnabled);
		file.set(SpawnerPaths.BREAKING_TOOL_TYPES.getPath(), toolTypes);
		file.set(SpawnerPaths.BREAKING_TOOL_ENCHANTS.getPath(), enchantTypes);
		file.set(SpawnerPaths.BREAKING_PERMS_ENABLED.getPath(), permEnabled);
		file.set(SpawnerPaths.BREAKING_PERMS_PERM.getPath(), permission);
		file.set(SpawnerPaths.BREAKING_IN_INVENTORY.getPath(), inInventory);
	}
	
	private String getToolName(Material item) {
		String type = item.name();
		type = type.toLowerCase();
		return type.replace("_", "").replace("%chainmail%", "").replace("diamond", "").replace("wooden", "")
				.replace("wood", "").replace("iron", "").replace("golden", "").replace("gold", "");
	}
}
