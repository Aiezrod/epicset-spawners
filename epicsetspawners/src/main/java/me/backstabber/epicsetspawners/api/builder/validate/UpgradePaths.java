package me.backstabber.epicsetspawners.api.builder.validate;

public enum UpgradePaths {

	ENABLED("upgrade.enabled", true), 
	LEVELS("upgrade.levels");
	private String path;
	private Object defaultData;

	UpgradePaths(String path) {
		this.path = path;
		this.defaultData = null;
	}

	UpgradePaths(String path, Object defaultData) {
		this.path = path;
		this.defaultData = defaultData;
	}

	public String getPath() {
		return this.path;
	}

	public Object getDefault() {
		return this.defaultData;
	}
}
