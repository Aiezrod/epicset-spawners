package me.backstabber.epicsetspawners.api.builder.validate;

import java.util.ArrayList;
import java.util.List;

public enum VariablePaths {
	CHANGE_BY("variable-settings.change-by","spawn"),
	PLACEHOLDER("variable-settings.placeholder","%vault_eco_balance%"),
	TIME("variable-settings.time-sec",60),
	VALUES("variable-settings.values","");
	private String path;
	private Object defaultData;

	VariablePaths(String path, Object defaultData) {
		this.path = path;
		this.defaultData = defaultData;
	}

	VariablePaths(String path, String... array) {
		this.path = path;
		List<String> data = new ArrayList<String>();
		for (String s : array)
			data.add(s);
		this.defaultData = data;
	}

	public String getPath() {
		return this.path;
	}

	public Object getDefault() {
		return this.defaultData;
	}
}
