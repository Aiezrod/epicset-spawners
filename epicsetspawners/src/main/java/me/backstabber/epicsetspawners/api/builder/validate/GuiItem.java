package me.backstabber.epicsetspawners.api.builder.validate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsetspawners.data.gui.GuiTriggers;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;

public class GuiItem {
	private GuiTriggers trigger;
	private String type;
	private String name;
	private List<String> lore;
	public GuiItem(GuiTriggers trigger,ItemStack item) {
		if(item.hasItemMeta()&&item.getItemMeta().hasItemFlag(ItemFlag.HIDE_ENCHANTS))
			type= "<glow>"+EpicMaterials.valueOf(item).name();
		else
			type= EpicMaterials.valueOf(item).name();
		name= CommonUtils.getItemName(item);
		if(item.hasItemMeta()&&item.getItemMeta().hasLore())
			lore=item.getItemMeta().getLore();
		else
			lore=new ArrayList<String>();
	}
	public GuiItem(GuiTriggers trigger,String type,String name,List<String> lore) {
		this.trigger=trigger;
		this.type=type;
		this.name=name;
		this.lore=lore;
	}
	public void apply(int slot,FileConfiguration file) {
		file.set(GuiPaths.ITEMS.getPath() + "." + slot + ".trigger", trigger.name());
		file.set(GuiPaths.ITEMS.getPath() + "." + slot + ".type", type);
		file.set(GuiPaths.ITEMS.getPath() + "." + slot + ".name", name);
		file.set(GuiPaths.ITEMS.getPath() + "." + slot + ".lore", lore);
	}
}
