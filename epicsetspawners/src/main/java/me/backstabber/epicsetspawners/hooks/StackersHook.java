package me.backstabber.epicsetspawners.hooks;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.PluginManager;

import com.bgsoftware.wildstacker.api.WildStackerAPI;
import com.google.inject.Inject;
import com.songoda.ultimatestacker.UltimateStacker;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.listeners.EntityListener;
import me.backstabber.epicsetspawners.listeners.ItemListener;
import uk.antiperson.stackmob.StackMob;
import uk.antiperson.stackmob.api.EntityManager;

public class StackersHook {
	@Inject
	private EpicSetSpawners plugin;
	@Inject
	private ItemListener itemListener;
	@Inject
	private EntityListener entityListener;

	private ItemHook itemHook;
	private MobHook mobHook;

	public void setup() {
		PluginManager pm = Bukkit.getPluginManager();
		if (pm.isPluginEnabled("WildStacker")) {
			itemHook = ItemHook.WILD_STACKER;
			mobHook = MobHook.WILD_STACKER;
			plugin.getLogger().log(Level.INFO,"Found WildStacker.Items & Mobs stacking will be handled by it.");
			plugin.getLogger().log(Level.INFO,"Spawner Features of WildStacker will be disabled.");
			return;
		} else if (pm.isPluginEnabled("UltimateStacker")) {
			itemHook = ItemHook.ULTIMATE_STACKER;
			mobHook = MobHook.ULTIMATE_STACKER;
			plugin.getLogger().log(Level.INFO,"Found UltimateStacker.Items & Mobs stacking will be handled by it.");
			plugin.getLogger().log(Level.INFO,"Spawner Features of UltimateStacker will be disabled.");
			return;
		} else if (pm.isPluginEnabled("StackMob")) {
			itemHook = ItemHook.SELF;
			mobHook = MobHook.STACKMOB;
			plugin.getLogger().log(Level.INFO,"Found StackMob.Mobs stacking will be handled by it.");
			plugin.getLogger().log(Level.INFO,"Item Stacking will be done by this plugin.");
			Bukkit.getPluginManager().registerEvents(itemListener, plugin);
			return;
		} else {
			itemHook = ItemHook.SELF;
			mobHook = MobHook.SELF;
			plugin.getLogger().log(Level.INFO,"No External Stacker found. Stacking will be handled by this plugin.");
			Bukkit.getPluginManager().registerEvents(itemListener, plugin);
			Bukkit.getPluginManager().registerEvents(entityListener, plugin);
			return;
		}
	}

	public int getStackSize(Entity entity) {
		if (entity instanceof Item) {
			switch (itemHook) {
			case SELF:
				return itemListener.getStackSize((Item) entity);

			case ULTIMATE_STACKER:
				UltimateStacker.getActualItemAmount((Item) entity);

			case WILD_STACKER:
				WildStackerAPI.getStackedItem((Item) entity).getStackAmount();
			default:
				return 1;
			}
		} else if (entity instanceof LivingEntity) {
			switch (mobHook) {
			case SELF:
				entityListener.getStackSize((LivingEntity) entity);

			case ULTIMATE_STACKER:
				UltimateStacker.getInstance().getEntityStackManager().getStack(entity).getAmount();

			case WILD_STACKER:
				WildStackerAPI.getStackedEntity((LivingEntity) entity).getStackAmount();

			case STACKMOB:
				EntityManager em = new EntityManager((StackMob) Bukkit.getPluginManager().getPlugin("StackMob"));
				em.getStackedEntity(entity).getSize();

			default:
				return 1;
			}
		}
		return 1;
	}

	public void setStackSize(Entity entity, int amount) {
		if(entity.hasMetadata("EpicSpawnerEntityBypass"))
			return;
		if (entity instanceof Item) {
			switch (itemHook) {
			case SELF:
				itemListener.setStackSize((Item) entity, amount);
				break;

			case ULTIMATE_STACKER:
				UltimateStacker.updateItemAmount((Item) entity, amount);
				break;

			case WILD_STACKER:
				WildStackerAPI.getStackedItem((Item) entity).setStackAmount(amount, true);
				break;
			default:
				break;
			}
		} else if (entity instanceof LivingEntity) {
			switch (mobHook) {
			case SELF:
				entityListener.setStackSize((LivingEntity) entity, amount);
				break;

			case ULTIMATE_STACKER:
				UltimateStacker.getInstance().getEntityStackManager().getStack(entity).setAmount(amount);
				break;

			case WILD_STACKER:
				WildStackerAPI.getStackedEntity((LivingEntity) entity).setStackAmount(amount, true);
				break;

			case STACKMOB:
				EntityManager em = new EntityManager((StackMob) Bukkit.getPluginManager().getPlugin("StackMob"));
				em.getStackedEntity(entity).setSize(amount);
				break;

			default:
				break;
			}
		}
	}

	public boolean isItemEnabled() {
		switch (itemHook) {

		case SELF:
			return itemListener.isEnabled();

		case ULTIMATE_STACKER:
			return true;

		case WILD_STACKER:
			return true;

		default:
			return false;
		}
	}

	public boolean isEntityEnabled() {
		switch (mobHook) {

		case SELF:
			return entityListener.isEnabled();

		case ULTIMATE_STACKER:
			return true;

		case WILD_STACKER:
			return true;

		case STACKMOB:
			return true;

		default:
			return false;
		}
	}

	private enum ItemHook {
		SELF, ULTIMATE_STACKER, WILD_STACKER;
	}

	private enum MobHook {
		SELF, STACKMOB, ULTIMATE_STACKER, WILD_STACKER;
	}
}
