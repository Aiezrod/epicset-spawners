package me.backstabber.epicsetspawners;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import me.backstabber.epicsetspawners.api.EpicSetSpawnersAPI;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.api.stores.spawner.VanillaStore;
import me.backstabber.epicsetspawners.command.SpawnerCommand;
import me.backstabber.epicsetspawners.command.subcommand.CreateCommand;
import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.listeners.EntityListener;
import me.backstabber.epicsetspawners.listeners.ItemListener;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.nms.impl.Version1_10;
import me.backstabber.epicsetspawners.nms.impl.Version1_11;
import me.backstabber.epicsetspawners.nms.impl.Version1_12;
import me.backstabber.epicsetspawners.nms.impl.Version1_13;
import me.backstabber.epicsetspawners.nms.impl.Version1_14;
import me.backstabber.epicsetspawners.nms.impl.Version1_15;
import me.backstabber.epicsetspawners.nms.impl.Version1_16;
import me.backstabber.epicsetspawners.nms.impl.Version1_8;
import me.backstabber.epicsetspawners.nms.impl.Version1_9;
import me.backstabber.epicsetspawners.stores.location.EpicLocationStore;
import me.backstabber.epicsetspawners.stores.location.StackedBlocks;
import me.backstabber.epicsetspawners.stores.spawner.BlockStore;
import me.backstabber.epicsetspawners.stores.spawner.CustomStore;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.stores.spawner.EpicVanillaStore;
import me.backstabber.epicsetspawners.stores.spawner.ItemStore;
import me.backstabber.epicsetspawners.stores.spawner.VariableStore;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class DepInjectorMod extends AbstractModule {

	private EpicSetSpawners plugin;
	private Injector injector;
	public DepInjectorMod(EpicSetSpawners plugin) {
		this.plugin = plugin;
		this.injector=Guice.createInjector(this);
	}
	public void injectMembers(Object object) {
		injector.injectMembers(object);;
	}
	@Override
	protected void configure() {
		this.bind(EpicSetSpawners.class).toInstance(plugin);
		EpicSpawnerStore store = new EpicSpawnerStore();
		this.bind(SpawnerStore.class).toInstance(store);
		this.bind(EpicSpawnerStore.class).toInstance(store);
		this.bind(VanillaStore.class).toInstance(new EpicVanillaStore());
		this.bind(ItemStore.class).toInstance(new ItemStore());
		this.bind(CustomStore.class).toInstance(new CustomStore());
		this.bind(VariableStore.class).toInstance(new VariableStore());
		this.bind(BlockStore.class).toInstance(new BlockStore());
		EpicLocationStore locations = new EpicLocationStore();
		this.bind(LocationStore.class).toInstance(locations);
		this.bind(EpicLocationStore.class).toInstance(locations);
		this.bind(ItemListener.class).toInstance(new ItemListener());
		this.bind(EntityListener.class).toInstance(new EntityListener());
		StackersHook hook = new StackersHook();
		this.bind(StackersHook.class).toInstance(hook);
		ApiImpl api = new ApiImpl();
		this.bind(ApiImpl.class).toInstance(api);
		this.bind(EpicSetSpawnersAPI.class).toInstance(api);
		NmsMethods nmsMethods;
		if (CommonUtils.getServerVersion() < 9)
			nmsMethods = new Version1_8(hook);
		else if (CommonUtils.getServerVersion() < 10)
			nmsMethods = new Version1_9(hook);
		else if (CommonUtils.getServerVersion() < 11)
			nmsMethods = new Version1_10(hook);
		else if (CommonUtils.getServerVersion() < 12)
			nmsMethods = new Version1_11(hook);
		else if (CommonUtils.getServerVersion() < 13)
			nmsMethods = new Version1_12(hook);
		else if (CommonUtils.getServerVersion() < 14)
			nmsMethods = new Version1_13(hook);
		else if (CommonUtils.getServerVersion() < 15)
			nmsMethods = new Version1_14(hook);
		else if (CommonUtils.getServerVersion() < 16)
			nmsMethods = new Version1_15(hook);
		else
			nmsMethods = new Version1_16(hook);
		this.bind(NmsMethods.class).toInstance(nmsMethods);
		CreateCommand create=new CreateCommand(plugin, store, nmsMethods);
		SpawnerCommand command = new SpawnerCommand(plugin, store,nmsMethods, locations, create);
		this.bind(SpawnerCommand.class).toInstance(command);
		this.bind(CreateCommand.class).toInstance(create);
		this.bind(StackedBlocks.class).toInstance(new StackedBlocks());
	}

}
