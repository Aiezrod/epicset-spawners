package me.backstabber.epicsetspawners.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.google.inject.Inject;

import me.backstabber.epicsetspawners.api.data.GuiData;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.data.gui.EpicGuiData;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;

public class GuiListener implements Listener {
	@Inject
	private LocationStore store;
	private Map<Player, SpawnerData> opened = new HashMap<Player, SpawnerData>();

	@EventHandler
	public void onGuiOpen(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			return;
		@SuppressWarnings("deprecation")
		ItemStack item = player.getInventory().getItemInHand();
		if (item == null || item.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial())) {
			Block block = event.getClickedBlock();
			if (!store.isStored(block))
				return;
			SpawnerData spawner = store.fetch(block);
			if(!spawner.getGuiData().isEnabled())
				return;
			spawner.getGuiData().openGui(player,block);
			opened.put(player, spawner);
		}

	}

	@EventHandler
	public void onGuiClick(InventoryClickEvent event) {
		if (!opened.containsKey((Player) event.getWhoClicked()))
			return;
		GuiData gui = opened.get((Player) event.getWhoClicked()).getGuiData();
		((EpicGuiData) gui).handleClick(event);
	}

	@EventHandler
	public void onGuiClose(InventoryCloseEvent event) {
		if (!opened.containsKey((Player) event.getPlayer()))
			return;
		GuiData gui = opened.get((Player) event.getPlayer()).getGuiData();
		opened.remove((Player) event.getPlayer());
		((EpicGuiData) gui).handleClose(event);
	}
}
