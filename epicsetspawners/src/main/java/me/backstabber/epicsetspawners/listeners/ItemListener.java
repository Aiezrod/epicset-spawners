package me.backstabber.epicsetspawners.listeners;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;

@SuppressWarnings("deprecation")
public class ItemListener implements Listener {

	@Inject
	private EpicSetSpawners plugin;

	public int getStackSize(Item item) {
		int amount = 0;
		if (item.hasMetadata("EpicSpawnerItem")) {
			try {
				amount = item.getMetadata("EpicSpawnerItem").get(0).asInt();
			} catch (Exception e) {
				amount = item.getItemStack().getAmount();
			}
		} else
			amount = item.getItemStack().getAmount();
		return amount;
	}

	public void setStackSize(Item item, int amount) {
		if (!isEnabled()) {
			item.getItemStack().setAmount(amount);
			return;
		}
		item.getItemStack().setAmount(1);
		item.setMetadata("EpicSpawnerItem", new FixedMetadataValue(plugin, (int) (amount)));
		String format = plugin.getSettings().getFile().getString("settings.item-stacker.format");
		format = format.replace("%amount%", String.valueOf(amount));
		format = format.replace("%item%", CommonUtils.getItemName(item.getItemStack()));
		item.setCustomName(CommonUtils.chat(format));
		item.setCustomNameVisible(true);
	}

	private int getMaxSize() {
		return plugin.getSettings().getFile().getInt("settings.item-stacker.max-stack");
	}

	public boolean isEnabled() {
		return plugin.getSettings().getFile().getBoolean("settings.item-stacker.enabled");
	}

	@EventHandler
	public void itemMerge(ItemMergeEvent event) {
		if (!isEnabled())
			return;
		event.setCancelled(true);
		Item from = event.getEntity();
		Item to = event.getTarget();
		if (getStackSize(from) >= getMaxSize() || getStackSize(to) >= getMaxSize())
			return;
		int totalAmount = getStackSize(to) + getStackSize(from);
		if (totalAmount > getMaxSize()) {
			setStackSize(from, totalAmount - getMaxSize());
			setStackSize(to, getMaxSize());
		} else {
			setStackSize(to, totalAmount);
			from.remove();
		}
	}

	@EventHandler
	public void itemSpawn(ItemSpawnEvent event) {
		if (!isEnabled())
			return;
		Item item = event.getEntity();
		int amount = getStackSize(item);
		setStackSize(item, amount);
	}

	public void inventoryPickup(InventoryPickupItemEvent event) {
		if (!isEnabled())
			return;
		Item item = event.getItem();
		int amount = getStackSize(item);
		event.setCancelled(true);
		item.getItemStack().setAmount(1);
		int freeSlots = getFreeSlots(event.getInventory(), item.getItemStack()); // get how many slots are empty
		if (amount <= (freeSlots * item.getItemStack().getMaxStackSize())) /// if more or enough space then required
		{

			item.getItemStack().setAmount(amount);
			event.getInventory().addItem(item.getItemStack());
			item.remove();
		} else if (freeSlots > 0) // if doesnt have enough space
		{
			item.getItemStack().setAmount((freeSlots * item.getItemStack().getMaxStackSize()));
			event.getInventory().addItem(item.getItemStack());
			amount = amount - (freeSlots * item.getItemStack().getMaxStackSize());
			setStackSize(item, amount);
		}
	}

	@EventHandler
	public void playerPickup(PlayerPickupItemEvent event) {
		if (!isEnabled())
			return;
		Item item = event.getItem();
		int amount = getStackSize(item);
		event.setCancelled(true);
		item.getItemStack().setAmount(1);
		Player player = event.getPlayer();
		int freeSlots = getFreeSlots(player.getInventory(), item.getItemStack()); // get how many slots player has empty
		if (amount <= (freeSlots * item.getItemStack().getMaxStackSize())) /// if he has more or enough space then
																			/// required
		{
			int fullStacks = (int) ((double) amount / (double) item.getItemStack().getMaxStackSize());
			int left = (int) ((int) amount - (((double) fullStacks) * item.getItemStack().getMaxStackSize()));
			ItemStack toGive = item.getItemStack();
			toGive.setAmount(toGive.getMaxStackSize());
			for (int i = 0; i < fullStacks; i++)
				player.getInventory().addItem(toGive.clone());
			toGive.setAmount(left);
			player.getInventory().addItem(toGive.clone());
			item.remove();
		} else if (freeSlots > 0) // if he doesnt have enough space
		{
			int fullStacks = freeSlots;
			ItemStack toGive = item.getItemStack();
			toGive.setAmount(toGive.getMaxStackSize());
			for (int i = 0; i < fullStacks; i++)
				player.getInventory().addItem(toGive.clone());
			player.getInventory().addItem(toGive.clone());
			amount = amount - (freeSlots * item.getItemStack().getMaxStackSize());
			setStackSize(item, amount);
		}
	}

	private static int getFreeSlots(Inventory inventory, ItemStack item) {
		int total = 0;
		ItemStack[] content = inventory.getContents();
		for (int i = 0; i < content.length; i++)
			if (content[i] != null && !content[i].getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
				total++;
		total = inventory.getSize() - total;
		return total;
	}

}
