package me.backstabber.epicsetspawners.listeners;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.stores.location.StackedBlocks;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;
import me.backstabber.epicsetspawners.utils.nbt.SafeNBT;

public class BlockListener implements Listener {

	@Inject
	private EpicSetSpawners plugin;
	@Inject
	private LocationStore locations;
	@Inject
	private SpawnerStore store;
	@Inject
	private StackedBlocks blocks;
	@Inject
	private StackersHook stackerHook;

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		ItemStack item = event.getPlayer().getInventory().getItemInHand();
		if (item == null || item.getType().equals(EpicMaterials.valueOf("AIR").getMaterial()))
			return;
		SafeNBT nbt = SafeNBT.get(item);
		String tag = nbt.getString("EpicSpawner");
		if (tag == null || tag.equals("")) {
			if(item.getType().equals(EpicMaterials.valueOf(UMaterials.SPAWNER).getMaterial())) {
				SpawnerData spawner = store.getSpawner(EntityType.PIG);
				((EpicSpawnerData) spawner).placeBlock(event);
			}
			return;
		}
		JsonParser parser=new JsonParser();
		JsonObject json=(JsonObject) parser.parse(tag);
		if(json.get("EpicSetSpawner")!=null) {
			SpawnerType type=SpawnerType.valueOf(json.get("Type").getAsString().toUpperCase());
			String name=json.get("Name").getAsString();
			try {
			SpawnerData spawner=SpawnerBuilder.load(type, name).getSpawner(json);
			((EpicSpawnerData) spawner).placeBlock(event);
			} catch(IllegalArgumentException e) {
				plugin.getLogger().log(Level.WARNING, event.getPlayer().getName()+" tried to place an unrecognized spawner.");
				SpawnerData spawner=store.getSpawner(EntityType.PIG);
				((EpicSpawnerData) spawner).placeBlock(event);
			}
			
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		if(blocks.isStored(block)) {
			int amount=blocks.fetch(block)-1;
			if(amount<=0) {
				blocks.removeBlock(block);
			}
			else {
				blocks.setBlock(block, amount);
				event.setCancelled(true);
				for(ItemStack drop:block.getDrops(event.getPlayer().getInventory().getItemInHand()))
					block.getWorld().dropItemNaturally(block.getLocation(), drop);
			}
		}
		if (!locations.isStored(block)) {
			if(block.getState() instanceof CreatureSpawner) {
				SpawnerData spawner = store.getSpawner(EntityType.PIG);
				((EpicSpawnerData) spawner).breakBlock(event);
			}
			return;
		}
		try {
		SpawnerData spawner = locations.fetch(block);
		((EpicSpawnerData) spawner).breakBlock(event);
		}catch(IllegalArgumentException e) {
			plugin.getLogger().log(Level.WARNING, "Unknown spawner found at "+CommonUtils.getBlockString(block.getLocation()));
			SpawnerData spawner=store.getSpawner(EntityType.PIG);
			((EpicSpawnerData) spawner).breakBlock(event);
		}
	}

	@EventHandler
	public void onBlockExplode(EntityExplodeEvent event) {
		for (Block block : event.blockList()) {
			if (locations.isStored(block)) {
				SpawnerData spawner = locations.fetch(block);
				((EpicSpawnerData) spawner).blockExplode(block);
			}
		}
	}

	@EventHandler
	public void onSpawnerSpawn(SpawnerSpawnEvent event) {
		Block block = event.getSpawner().getBlock();
		Entity entity = event.getEntity();
		if (!locations.isStored(block))
			return;
		SpawnerData spawner = locations.fetch(block);
		if (!spawner.isEnabled()) {
			event.setCancelled(true);
			return;
		}
		if (spawner instanceof EpicSpawnerData) {
			if(!((EpicSpawnerData)spawner).canSpawn(block, event.getEntity())) {
				event.getEntity().remove();
				return;
			}
			if(spawner.getType().equals(SpawnerType.CUSTOM_MOB))
				return;
			int amount = ((EpicSpawnerData) spawner).getAmount();
			if (entity instanceof Item && stackerHook.isItemEnabled())
				stackerHook.setStackSize(entity, amount);
			else if (entity instanceof LivingEntity && stackerHook.isEntityEnabled())
				stackerHook.setStackSize(entity, amount);
		}
	}
	@EventHandler
	public void onSpawnerChange(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			Block block=event.getClickedBlock();
			ItemStack item=event.getItem();
			if(item!=null&&item.getType().equals(EpicMaterials.valueOf(UMaterials.SPAWN_EGG).getMaterial())&&block.getState() instanceof CreatureSpawner)
			{
				event.setCancelled(true);
			}
			if(event.getPlayer().isSneaking()&&Bukkit.getPluginManager().isPluginEnabled("WildStacker")&&block.getState() instanceof CreatureSpawner) {
				new BukkitRunnable() {
					@Override
					public void run() {
						event.getPlayer().closeInventory();
					}
				}.runTaskLater(plugin, 1);
			}
		}
	}
}
