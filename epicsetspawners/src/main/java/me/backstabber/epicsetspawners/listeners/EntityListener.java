package me.backstabber.epicsetspawners.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.metadata.FixedMetadataValue;

import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class EntityListener implements Listener {

	@Inject
	private EpicSetSpawners plugin;

	public int getStackSize(LivingEntity entity) {
		int amount = 0;
		if (entity.hasMetadata("EpicSpawnerEntity")) {
			try {
				amount = entity.getMetadata("EpicSpawnerEntity").get(0).asInt();
			} catch (Exception e) {
				amount = 1;
			}
		} else
			amount = 1;
		return amount;
	}

	public void setStackSize(LivingEntity entity, int amount) {
		if (!isEnabled()) {
			return;
		}

		entity.setMetadata("EpicSpawnerEntity", new FixedMetadataValue(plugin, (int) (amount)));
		String format = plugin.getSettings().getFile().getString("settings.mob-stacker.format");
		format = format.replace("%amount%", String.valueOf(amount));
		format = format.replace("%entity%", CommonUtils.format(entity.getType().name()));
		entity.setCustomName(CommonUtils.chat(format));
		entity.setCustomNameVisible(true);
	}

	public boolean isEnabled() {
		return plugin.getSettings().getFile().getBoolean("settings.mob-stacker.enabled");
	}

	private int getMaxSize() {
		return plugin.getSettings().getFile().getInt("settings.mob-stacker.max-stack");
	}

	@EventHandler
	public void onSpawn(CreatureSpawnEvent event) {
		if (!event.getSpawnReason().equals(SpawnReason.SPAWNER))
			return;
		LivingEntity spawnedEntity = event.getEntity();
		if(spawnedEntity.hasMetadata("EpicSpawnerEntityBypass"))
			return;
		for (Entity entity : spawnedEntity.getWorld().getNearbyEntities(spawnedEntity.getLocation(), 7, 7, 7)) {
			if (!entity.getType().equals(spawnedEntity.getType()))
				continue;
			if (!(entity instanceof LivingEntity))
				continue;
			int newSize = getStackSize(spawnedEntity) + getStackSize((LivingEntity) entity);
			if (newSize > getMaxSize()) {
				setStackSize(spawnedEntity, newSize - getMaxSize());
				setStackSize((LivingEntity) entity, getMaxSize());
			} else {
				setStackSize((LivingEntity) entity, newSize);
			}
			spawnedEntity.remove();
			return;
		}
	}

	@EventHandler
	public void onDeath(EntityDeathEvent event) {
		LivingEntity died = event.getEntity();
		if (getStackSize(died) <= 1)
			return;
		int newSize = getStackSize(died) - 1;
		Entity spawned = died.getWorld().spawnEntity(died.getLocation(), died.getType());
		setStackSize((LivingEntity) spawned, newSize);
	}
}
