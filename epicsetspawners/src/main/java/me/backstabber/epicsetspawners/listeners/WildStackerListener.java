package me.backstabber.epicsetspawners.listeners;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import com.bgsoftware.wildstacker.api.events.SpawnerPlaceEvent;
import com.google.inject.Inject;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;

public class WildStackerListener implements Listener {
	@Inject
	private EpicSetSpawners plugin;
	@Inject
	private LocationStore locations;
	@EventHandler
	public void onPlace(SpawnerPlaceEvent event) {
		event.setCancelled(true);
		Block block=event.getSpawner().getLocation().getBlock();
		if(locations.isStored(block)) {
			new BukkitRunnable() {
				@Override
				public void run() {
					locations.fetch(block).applyToBlock(block, null);
				}
			}.runTaskLater(plugin, 2);
		}
	}
}
