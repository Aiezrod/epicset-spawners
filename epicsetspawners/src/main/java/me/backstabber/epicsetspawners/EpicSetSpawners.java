package me.backstabber.epicsetspawners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.inject.Inject;

import me.backstabber.epicsetspawners.api.EpicSetSpawnersAPI;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SpawnerCommand;
import me.backstabber.epicsetspawners.command.subcommand.CreateCommand;
import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.listeners.BlockListener;
import me.backstabber.epicsetspawners.listeners.GuiListener;
import me.backstabber.epicsetspawners.listeners.WildStackerListener;
import me.backstabber.epicsetspawners.stores.location.EpicLocationStore;
import me.backstabber.epicsetspawners.stores.location.StackedBlocks;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.YamlManager;

/*
 * TODO
 * make command & command aliases using interface
 * 
 */
public class EpicSetSpawners extends JavaPlugin {

	@Inject
	private SpawnerStore store;
	@Inject
	private LocationStore locations;
	@Inject
	private StackedBlocks blocks;
	@Inject
	private BlockListener blockListener;
	@Inject
	private GuiListener guiListener;
	@Inject
	private StackersHook stackersHook;
	@Inject
	private SpawnerCommand command;
	@Inject
	private CreateCommand create;
	@Inject
	private WildStackerListener wildListener;
	private YamlManager config;
	private DepInjectorMod module;

	private static EpicSetSpawnersAPI api;

	@Override
	public void onEnable() {
		config = new YamlManager(this, "config");
		module = new DepInjectorMod(this);
		module.injectMembers(this);
		stackersHook.setup();
		((EpicSpawnerStore) store).setup();
		((EpicLocationStore) locations).setup();
		blocks.setup();
		Bukkit.getPluginManager().registerEvents(guiListener, this);
		Bukkit.getPluginManager().registerEvents(blockListener, this);
		Bukkit.getPluginManager().registerEvents(create, this);
		if (Bukkit.getPluginManager().isPluginEnabled("WildStacker")) {
			Bukkit.getPluginManager().registerEvents(wildListener, this);
		}
		getCommand("espawner").setExecutor(command);
		getCommand("espawner").setTabCompleter(command);
		api = new ApiImpl();
		((ApiImpl) api).setup(this, stackersHook, store, locations);
	}
	@Override
		public void onDisable() {
			for(Player player:Bukkit.getOnlinePlayers()) {
				if(player.hasMetadata("EpicSpawnerBuilding"))
					player.removeMetadata("EpicSpawnerBuilding", this);
			}
		}
	public void injectMembers(Object object) {
		module.injectMembers(object);
	}
	public YamlManager getSettings() {
		return config;
	}

	public static EpicSetSpawnersAPI getApi() {
		return api;
	}
}
